//
//  StopWatchViewController.m
//  Precision iOS
//
//  Created by Zach Kauffman on 7/21/14.
//  Copyright (c) 2014 Zach Kauffman. All rights reserved.
//

#import "StopWatchViewController.h"

@interface StopWatchViewController ()

@end

@implementation StopWatchViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.delegate = self.presentingViewController;
    _running = NO;
    _totalElapsed = 0;
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)startPausePressed:(id)sender
{
    if(!_running) {  //start the time
        [self.startPauseImage setImage:[UIImage imageNamed:@"pause.png"]];
        _startTime = [NSDate timeIntervalSinceReferenceDate];
        _running = YES;
        _alreadyStarted = YES;
        [sender setTitle:@"Pause" forState:UIControlStateNormal];
        [self updateTime];
    } else {        //pause the timer
        NSTimeInterval elapsed = [NSDate timeIntervalSinceReferenceDate] - _startTime;
        [self.startPauseImage setImage:[UIImage imageNamed:@"play.png"]];
        _totalElapsed += elapsed;
        [sender setTitle:@"Start" forState:UIControlStateNormal];
        _running = NO;
    }
    
}

-(IBAction)clearPressed:(id)sender
{
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(updateTime) object:self];
    _running = NO;
    _totalElapsed = 0;
    _alreadyStarted = NO;
    _timerLabel.text = @"00:00:00";
    [_startBtn setTitle:@"Start" forState:UIControlStateNormal];
    [self.startPauseImage setImage:[UIImage imageNamed:@"play.png"]];
}

-(IBAction)donePressed:(id)sender
{
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(updateTime) object:self];
    NSString *timerVal;
    if(_running) {
        _running = NO;
        NSTimeInterval currentTime = [NSDate timeIntervalSinceReferenceDate];
        NSTimeInterval elapsed = currentTime - _startTime + _totalElapsed;
        timerVal = [self getTimerString:elapsed];
    } else {
        _running = NO;
        timerVal = [self getTimerString:_totalElapsed];
    }
    NSArray *ref = [((UINavigationController*)self.delegate) viewControllers];
    NewEntryViewController *parent = [ref objectAtIndex:[ref count]-1];
    [parent setTimerValue:timerVal];
}

-(void)updateTime
{
    if(!_running) return;
    
    NSTimeInterval currentTime = [NSDate timeIntervalSinceReferenceDate];
    NSTimeInterval elapsed = currentTime - _startTime + _totalElapsed;
    
    int hours = (int) elapsed/3600.0;
    int mins = (int) (elapsed - hours*3600)/60.0;
    int seconds = (int) elapsed%60;
    
    _timerLabel.text = [NSString stringWithFormat:@"%02u:%02u:%02u", hours, mins, seconds];
    [self performSelector:@selector(updateTime) withObject:self afterDelay:1];
}

-(NSString*)getTimerString: (NSTimeInterval)elapsed
{
    int hours = (int) elapsed/3600.0;
    double minutes = (elapsed - hours*3600)/60.0;
    int mins = 0;
    if(minutes < 9) {
        mins = 6;
    } else if(minutes < 13.5) {
        mins = 12;
    } else if(minutes < 16.5) {
        mins = 15;
    } else if(minutes < 21) {
        mins = 18;
    } else if(minutes < 27) {
        mins = 24;
    } else if(minutes < 33) {
        mins = 30;
    } else if(minutes < 39) {
        mins = 36;
    } else if(minutes < 43.5) {
        mins = 42;
    } else if(minutes < 46.5) {
        mins = 45;
    } else if(minutes < 51) {
        mins = 48;
    } else {
        mins = 54;
    }
    
    return [NSString stringWithFormat:@"%u:%02u", hours, mins];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
