//
//  MatterObject.h
//  Precision iOS
//
//  Created by Zach Kauffman on 6/1/14.
//  Copyright (c) 2014 Zach Kauffman. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MatterObject : NSObject
@property (nonatomic, strong) NSString *matterName;
@property (nonatomic, strong) NSString *matterId;
@property (nonatomic, strong) NSString *tGroup;
@property (nonatomic, strong) NSString *cGroup;
-(MatterObject*) initWithId: (NSString*)matterId andName: (NSString*)matterName
              taskCodeGroup:(NSString*) tGroup componentCodeGroup:(NSString*)cGroup;
@end
