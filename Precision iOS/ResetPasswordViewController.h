//
//  ResetPasswordViewController.h
//  Precision iOS
//
//  Created by Zach Kauffman on 6/11/14.
//  Copyright (c) 2014 Zach Kauffman. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ResetPasswordViewController : UIViewController<UITextFieldDelegate, UIScrollViewDelegate>
@property (strong, nonatomic) IBOutlet UITextField *usernameTextfield;
@property (strong, nonatomic) IBOutlet UITextField *emailTextfield;
@property (strong, nonatomic) NSMutableData *responseData;
@property (strong, nonatomic) UIActivityIndicatorView *spinner;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property BOOL isScrolled;
-(IBAction)resetBtnPressed:(id)sender;
-(IBAction)loginBtnPressed:(id)sender;
@end
