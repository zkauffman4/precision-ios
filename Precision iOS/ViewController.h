//
//  ViewController.h
//  Precision iOS
//
//  Created by Zach Kauffman on 5/29/14.
//  Copyright (c) 2014 Zach Kauffman. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController<UITextFieldDelegate, UIScrollViewDelegate>
@property (weak, nonatomic) IBOutlet UITextField *txt_username;
@property (weak, nonatomic) IBOutlet UITextField *txt_password;
@property (strong, nonatomic) UIActivityIndicatorView *spinner;
@property (strong, nonatomic) NSMutableData *responseData;
@property (strong, nonatomic) IBOutlet UIButton *demoBtn;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property BOOL isScrolled;

-(IBAction)loginBtn:(id)sender;
-(IBAction)resetBtnPressed:(id)sender;
-(IBAction)createBtnPressed:(id)sender;
-(IBAction)demoBtnPressed:(id)sender;
@end
