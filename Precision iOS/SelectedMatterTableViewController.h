//
//  SelectedMatterTableViewController.h
//  Precision iOS
//
//  Created by Zach Kauffman on 6/3/14.
//  Copyright (c) 2014 Zach Kauffman. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWTableViewCell.h"

@interface SelectedMatterTableViewController : UITableViewController<SWTableViewCellDelegate>

@property (nonatomic, strong) NSMutableData *responseData;
@property (nonatomic, strong) NSMutableArray *entryArray;
@property (nonatomic, strong) NSString *matter;
@end
