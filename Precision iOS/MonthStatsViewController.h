//
//  MonthStatsViewController.h
//  Precision iOS
//
//  Created by Zach Kauffman on 6/4/14.
//  Copyright (c) 2014 Zach Kauffman. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MonthStatsViewController : UIViewController
@property (strong, nonatomic) IBOutlet UIView *hourView;
@property (strong, nonatomic) IBOutlet UIView *entriesView;
@property (strong, nonatomic) IBOutlet UIView *mattersView;
@property (strong, nonatomic) IBOutlet UIView *goalView;
@property (strong, nonatomic) IBOutlet UILabel *hoursLabel;
@property (strong, nonatomic) IBOutlet UILabel *entriesLabel;
@property (strong, nonatomic) IBOutlet UILabel *mattersLabel;
@property (strong, nonatomic) IBOutlet UIProgressView *goalProgressBar;
@property (strong, nonatomic) IBOutlet UILabel *goalProgressLabel;
@property (strong, nonatomic) IBOutlet UIImageView *hoursByMatterImage;
@property (strong, nonatomic) IBOutlet UILabel *hoursByMatterLabel;
@property (strong, nonatomic) IBOutlet UIImageView *entriesByMatterImage;
@property (strong, nonatomic) IBOutlet UILabel *entriesByMatterLabel;
@property (strong, nonatomic) IBOutlet UIImageView *hoursByMonthImage;
@property (strong, nonatomic) IBOutlet UILabel *hoursByMonthLabel;
@property (strong, nonatomic) IBOutlet UIImageView *entriesByMonthImage;
@property (strong, nonatomic) IBOutlet UILabel *entriesByMonthLabel;
@property (strong, nonatomic) IBOutlet UIImageView *mattersByMonthImage;
@property (strong, nonatomic) IBOutlet UILabel *mattersByMonthLabel;
@property (strong, nonatomic) NSMutableData *responseData;
@property (strong, nonatomic) UIActivityIndicatorView *spinner;
@end
