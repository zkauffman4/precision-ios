//
//  SelectedMatterTableViewController.m
//  Precision iOS
//
//  Created by Zach Kauffman on 6/3/14.
//  Copyright (c) 2014 Zach Kauffman. All rights reserved.
//

#import "SelectedMatterTableViewController.h"
#import "EntryObject.h"
#import "CustomTableViewCell.h"
#import "AppDelegate.h"
#import "ViewController.h"
#import "MatterObject.h"
#import "NewEntryViewController.h"

@interface SelectedMatterTableViewController ()

@end

@implementation SelectedMatterTableViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSString *url = [NSString stringWithFormat: @"https://precisiontimeentry.com/api/get_matter_history.php?matter_id=%@", _matter];
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:url]];
    
    // Create url connection and fire request
    NSURLConnection *conn = [[NSURLConnection alloc] initWithRequest:request delegate:self];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
#warning Potentially incomplete method implementation.
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
#warning Incomplete method implementation.
    // Return the number of rows in the section.
    return [_entryArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"SubCell";
    CustomTableViewCell *cell = (CustomTableViewCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    // Add utility buttons
    NSMutableArray *leftUtilityButtons = [NSMutableArray new];
    NSMutableArray *rightUtilityButtons = [NSMutableArray new];
    [rightUtilityButtons sw_addUtilityButtonWithColor:
     [UIColor colorWithRed:137/255.0f green:207/255.0f blue:240/255.0f alpha:1]
                                                title:@"Clone"];
    
    cell.rightUtilityButtons = rightUtilityButtons;
    cell.delegate = self;
    
    
    if([_entryArray count] == 0){
        cell.matterNameLabel.text = @"No submittable entries";
    }
    else{
        EntryObject *currentEntry = [_entryArray objectAtIndex:indexPath.row];
        cell.matterNameLabel.text = [currentEntry matterName];
        cell.matterDescriptionLabel.text = [currentEntry description];
        cell.dateWorkedLabel.text = [currentEntry date];
        cell.hoursWorkedLabel.text = [currentEntry hours];
        // in the future you can grab whatever data you need like this
        //[currentPlace placeName], or [currentPlace placeDay];
        
    }
    
    return cell;
    
}

-(void)didOpen:(SWTableViewCell *)cell
{
    for(int i = 0; i < [self.tableView numberOfRowsInSection:0]; i++) {
        NSIndexPath *mypath = [NSIndexPath indexPathForRow:i inSection:0];
        SWTableViewCell *cellTest = (SWTableViewCell*)[self.tableView cellForRowAtIndexPath:mypath];
        if(cellTest != cell)
            [cellTest hideUtilityButtonsAnimated:YES];
    }
}

- (void)swipeableTableViewCell:(SWTableViewCell *)cell didTriggerRightUtilityButtonWithIndex:(NSInteger)index {
    
    switch (index) {
        case 0:
        {
            EntryObject* entry = (EntryObject*)[_entryArray objectAtIndex:[self.tableView indexPathForCell:cell].row];
            MatterObject *matter = [[MatterObject alloc] initWithId:entry.matterId andName:entry.matterName taskCodeGroup:entry.lcodeGroup componentCodeGroup:entry.acodeGroup];
            AppDelegate *appDelegate;
            appDelegate=(AppDelegate *)[[UIApplication sharedApplication] delegate];//in didLoad method
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:appDelegate.storyboardName bundle:nil];
            NewEntryViewController *v = [storyboard instantiateViewControllerWithIdentifier:@"NewEntryView"];
            [v setCloningEntry:entry];
            [v setMatter:matter];
            v.te = @"1";
            [self.navigationController pushViewController:v animated:YES];
            break;
        }
    }
}

-(void)setupPlacesFromJSONArray:(NSData*)dataFromServerArray{
    NSError *error;
    
    NSDictionary *dictFromServer = [NSJSONSerialization JSONObjectWithData:dataFromServerArray options:0 error:&error];
    
    if(error){
        NSLog(@"error parsing the json data from server with error description - %@", [error localizedDescription]);
    }
    else {
        NSArray *arrayFromServer = [dictFromServer objectForKey:@"entries"];
        _entryArray = [[NSMutableArray alloc] init];
        for(NSDictionary *singleEntry in arrayFromServer)
        {
            EntryObject *entry = [[EntryObject alloc] initWithJSONData:singleEntry];
            [_entryArray addObject:entry];
        }
        
    }
}

#pragma mark NSURLConnection Delegate Methods

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    // A response has been received, this is where we initialize the instance var you created
    // so that we can append data to it in the didReceiveData method
    // Furthermore, this method is called each time there is a redirect so reinitializing it
    // also serves to clear it
    _responseData = [[NSMutableData alloc] init];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    // Append the new data to the instance variable you declared
    [_responseData appendData:data];
}

- (NSCachedURLResponse *)connection:(NSURLConnection *)connection
                  willCacheResponse:(NSCachedURLResponse*)cachedResponse {
    // Return nil to indicate not necessary to store a cached response for this connection
    return nil;
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    // The request is complete and data has been received
    // You can parse the stuff in your instance variable now
    
    NSError *jsonParsingError = nil;
    id object = [NSJSONSerialization JSONObjectWithData:_responseData options:0 error:&jsonParsingError];
    
    if (jsonParsingError) {
        NSLog(@"JSON ERROR: %@", [jsonParsingError localizedDescription]);
    } else {
        if([object isKindOfClass:[NSDictionary class]])
        {
            [self setupPlacesFromJSONArray:_responseData];
            [[self tableView] reloadData];
        } else if([object isKindOfClass:[NSDictionary class]]) {
            if([object objectForKey:@"session"] != nil) {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Session Timeout"
                                                                message:@"You have had more than 15 minutes of inactivity. To protect your account, you have been logged out. Please log in again."
                                                               delegate:nil
                                                      cancelButtonTitle:@"OK"
                                                      otherButtonTitles:nil];
                [alert show];
                AppDelegate *appDelegate;
                appDelegate=(AppDelegate *)[[UIApplication sharedApplication] delegate];//in didLoad method
                UIStoryboard *storyboard = [UIStoryboard storyboardWithName:appDelegate.storyboardName bundle:nil];
                ViewController *v = [storyboard instantiateViewControllerWithIdentifier:@"loginController"];
                [self presentViewController:v animated:YES completion:nil];
            }
        }

    }
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    // The request has failed for some reason!
    // Check the error var
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Login error"
                                                    message:[error localizedDescription]
                                                   delegate:nil
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];
}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a story board-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}

 */

@end
