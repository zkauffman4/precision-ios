//
//  MattersHistoryViewController.h
//  Precision iOS
//
//  Created by Zach Kauffman on 6/3/14.
//  Copyright (c) 2014 Zach Kauffman. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MattersHistoryViewController : UITableViewController

@property (nonatomic, strong) NSMutableData *responseData;
@property (nonatomic, strong) NSMutableArray *entryArray;

@end
