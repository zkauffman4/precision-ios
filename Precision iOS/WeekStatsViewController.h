//
//  WeekStatsViewController.h
//  Precision iOS
//
//  Created by Zach Kauffman on 6/4/14.
//  Copyright (c) 2014 Zach Kauffman. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WeekStatsViewController : UIViewController
@property (strong, nonatomic) IBOutlet UIView *hoursView;
@property (strong, nonatomic) IBOutlet UIView *entriesView;
@property (strong, nonatomic) IBOutlet UIView *mattersView;
@property (strong, nonatomic) IBOutlet UIView *goalView;
@property (strong, nonatomic) IBOutlet UIProgressView *goalProgressBar;
@property (strong, nonatomic) IBOutlet UILabel *hoursLabel;
@property (strong, nonatomic) IBOutlet UILabel *entriesLabel;
@property (strong, nonatomic) IBOutlet UILabel *mattersLabel;
@property (strong, nonatomic) IBOutlet UILabel *goalProgressLabel;
@property (strong, nonatomic) IBOutlet UIImageView *hoursByMatterImage;
@property (strong, nonatomic) IBOutlet UILabel *hoursByMatterLabel;
@property (strong, nonatomic) IBOutlet UIImageView *entriesByMatterImage;
@property (strong, nonatomic) IBOutlet UILabel *entriesByMatterLabel;
@property (strong, nonatomic) IBOutlet UIImageView *hoursByWeekImage;
@property (strong, nonatomic) IBOutlet UILabel *hoursByWeekLabel;
@property (strong, nonatomic) IBOutlet UIImageView *entriesByWeekImage;
@property (strong, nonatomic) IBOutlet UILabel *entriesByWeekLabel;
@property (strong, nonatomic) IBOutlet UIImageView *mattersByWeekImage;
@property (strong, nonatomic) IBOutlet UILabel *mattersByWeekLabel;
@property (strong, nonatomic) NSMutableData *responseData;
@property (strong, nonatomic) UIActivityIndicatorView *spinner;
@end
