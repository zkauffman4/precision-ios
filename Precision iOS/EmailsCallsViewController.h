//
//  EmailsCallsViewController.h
//  Precision iOS
//
//  Created by Zach Kauffman on 6/6/14.
//  Copyright (c) 2014 Zach Kauffman. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EmailsCallsViewController : UITableViewController

@property (strong, nonatomic) NSMutableArray *inbox;
@property (strong, nonatomic) NSMutableArray *sent;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *submitEntriesBtn;
@property (nonatomic, strong) NSMutableArray * selectedItems;
@property (strong, nonatomic) IBOutlet UIToolbar *submitEntriesBar;
@property (strong, nonatomic) NSMutableData *responseData;
@property (strong, nonatomic) NSMutableArray *selectedPaths;

@end
