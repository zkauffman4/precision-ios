//
//  HomeTableViewController.h
//  Precision iOS
//
//  Created by Zach Kauffman on 5/29/14.
//  Copyright (c) 2014 Zach Kauffman. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWTableViewCell.h"

@interface HomeTableViewController : UITableViewController<SWTableViewCellDelegate>

@property (strong, nonatomic) IBOutlet UIToolbar *submitEntriesBar;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *submitEntriesBtn;
@property (nonatomic, strong) NSMutableArray * submittables;
@property (nonatomic, strong) NSMutableData *responseData;
@property (nonatomic, strong) NSMutableArray *entryArray;
@property (nonatomic, strong) NSMutableArray *spotsInArrayToRemove;
@property (nonatomic, strong) NSIndexPath *currentIndexPath;
@property (nonatomic, strong) NSMutableArray *selectedItems;
@property (nonatomic, strong) UIActivityIndicatorView *spinner;
@property BOOL panning;
-(void)refreshData;

@end
