//
//  TaskCodeObject.m
//  Precision iOS
//
//  Created by Zach Kauffman on 6/2/14.
//  Copyright (c) 2014 Zach Kauffman. All rights reserved.
//

#import "TaskCodeObject.h"

@implementation TaskCodeObject

-(TaskCodeObject*) initWithId: (NSString*)codeId andName: (NSString*)codeName
{
    self = [TaskCodeObject alloc];
    
    self.codeId = codeId;
    self.codeName = codeName;
    
    return self;
}

@end
