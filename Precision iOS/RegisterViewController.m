//
//  RegisterViewController.m
//  Precision iOS
//
//  Created by Zach Kauffman on 6/11/14.
//  Copyright (c) 2014 Zach Kauffman. All rights reserved.
//

#import "RegisterViewController.h"

@interface RegisterViewController ()

@end

@implementation RegisterViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    _isScrolled = NO;
    self.usernameTextfield.delegate = self;
    self.emailTextfield.delegate = self;
    self.confirmEmailTextfield.delegate = self;
    self.passwordTextfield.delegate = self;
    self.confirmPasswordTextfield.delegate = self;
    self.registrationCodeTextfield.delegate = self;
    self.billingIdTextfield.delegate = self;
    
    self.usernameTextfield.borderStyle = UITextBorderStyleRoundedRect;
    self.emailTextfield.borderStyle = UITextBorderStyleRoundedRect;
    self.confirmEmailTextfield.borderStyle = UITextBorderStyleRoundedRect;
    self.passwordTextfield.borderStyle = UITextBorderStyleRoundedRect;
    self.confirmPasswordTextfield.borderStyle = UITextBorderStyleRoundedRect;
    self.registrationCodeTextfield.borderStyle = UITextBorderStyleRoundedRect;
    self.billingIdTextfield.borderStyle = UITextBorderStyleRoundedRect;
    
	// Do any additional setup after loading the view.
    
    self.confirmPasswordTextfield.secureTextEntry = YES;
    self.passwordTextfield.secureTextEntry = YES;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    [self scrollBack];
    return YES;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    [self scrollViewToCenterOfScreen:textField];
    return YES;
    
}

-(BOOL) textFieldShouldEndEditing:(UITextField *)textField
{
    
    return YES;
}

- (void)scrollViewToCenterOfScreen:(UIView *)theView {
    CGFloat viewCenterY = theView.center.y;
    CGRect applicationFrame = [[UIScreen mainScreen] applicationFrame];
    
    CGFloat availableHeight = applicationFrame.size.height - 200;            // Remove area covered by keyboard
    
    CGFloat y = viewCenterY - availableHeight / 2.0;
    if (y < 0) {
        y = 0;
    }
    _isScrolled = YES;
    
    [self.scrollView setContentOffset:CGPointMake(0, y) animated:YES];
    
}

- (void) scrollBack
{
    if ( ! _isScrolled ) return;
    [self.scrollView setContentOffset:CGPointMake(0, 0) animated:YES];
    _isScrolled = NO;
}


-(IBAction)backBtnPressed:(id)sender
{
    [self performSegueWithIdentifier:@"regToLogin" sender:self];
}

-(IBAction)createBtnPressed:(id)sender
{
    if(![_emailTextfield.text isEqualToString:_confirmEmailTextfield.text]) {
        //email mismatch
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Registration Error"
                                                        message:@"Emails do not match!"
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
    } else if(![_passwordTextfield.text isEqualToString:_confirmPasswordTextfield.text]) {
        //password mismatch
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Registration Error"
                                                        message:@"Passwords do not match!"
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
    } else if([_emailTextfield.text isEqualToString:@""] || [_confirmEmailTextfield.text isEqualToString:@""] || [_usernameTextfield.text isEqualToString:@""] || [_passwordTextfield.text isEqualToString:@""] || [_confirmPasswordTextfield.text isEqualToString:@""] || [_registrationCodeTextfield.text isEqualToString:@""] || [_billingIdTextfield.text isEqualToString:@""]) {
        //not enough info
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Registration Error"
                                                        message:@"All fields are required!"
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
    } else if([_passwordTextfield.text length] < 7) {
        //password not long enough
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Registration Error"
                                                        message:@"Password must be at least 7 characters!"
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
    } else if(![self NSStringIsValidEmail:_emailTextfield.text]) {
        //regex mismatch email
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Registration Error"
                                                        message:@"Email is not valid!"
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
    } else {
        //success, post data to server
        _spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        _spinner.opaque = NO;
        _spinner.backgroundColor = [UIColor colorWithWhite:0.0f alpha:0.6f];
        [_spinner setBounds:self.view.bounds];
        _spinner.center = self.view.center;
        _spinner.activityIndicatorViewStyle = UIActivityIndicatorViewStyleGray;
        [_spinner setColor:[UIColor colorWithRed:0.6 green:0.8 blue:1.0 alpha:1.0]];
        [self.view addSubview:_spinner]; // spinner is not visible until started
        [_spinner startAnimating];
        NSString *url = [NSString stringWithFormat: @"https://precisiontimeentry.com/api/create_account.php"];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]];
        [request setHTTPMethod:@"POST"];
        NSString *postString = [NSString stringWithFormat:@"username=%@&email=%@&password=%@&billingId=%@&regCode=%@", _usernameTextfield.text, _emailTextfield.text, _passwordTextfield.text, _billingIdTextfield.text, _registrationCodeTextfield.text];
        [request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
        
        // Create url connection and fire request
        NSURLConnection *conn = [[NSURLConnection alloc] initWithRequest:request delegate:self];

        
    }
}

#pragma mark NSURLConnection Delegate Methods

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    // A response has been received, this is where we initialize the instance var you created
    // so that we can append data to it in the didReceiveData method
    // Furthermore, this method is called each time there is a redirect so reinitializing it
    // also serves to clear it
    _responseData = [[NSMutableData alloc] init];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    // Append the new data to the instance variable you declared
    [_responseData appendData:data];
}

- (NSCachedURLResponse *)connection:(NSURLConnection *)connection
                  willCacheResponse:(NSCachedURLResponse*)cachedResponse {
    // Return nil to indicate not necessary to store a cached response for this connection
    return nil;
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    // The request is complete and data has been received
    // You can parse the stuff in your instance variable now
    [_spinner stopAnimating];
    NSError *jsonParsingError = nil;
    id object = [NSJSONSerialization JSONObjectWithData:_responseData options:0 error:&jsonParsingError];
    
    if (jsonParsingError) {
        NSLog(@"JSON ERROR: %@", [jsonParsingError localizedDescription]);
    } else {
        if([object isKindOfClass:[NSDictionary class]])
        {
            int responseCode = [[object objectForKey:@"response_code"] intValue];
            if(responseCode == 5) {
                [self performSegueWithIdentifier:@"regToLogin" sender:self];
            } else if(responseCode == 1) {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Registration error"
                                                                message:@"That username already exists, please change and try again"
                                                               delegate:nil
                                                      cancelButtonTitle:@"OK"
                                                      otherButtonTitles:nil];
                [alert show];
            } else if(responseCode == 2) {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Registration error"
                                                                message:@"This email already exists, please change and try again"
                                                               delegate:nil
                                                      cancelButtonTitle:@"OK"
                                                      otherButtonTitles:nil];
                [alert show];
            } else if(responseCode == 3) {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Registration error"
                                                                message:@"The registration code you entered was invalid, please try again"
                                                               delegate:nil
                                                      cancelButtonTitle:@"OK"
                                                      otherButtonTitles:nil];
                [alert show];
            } else if(responseCode == 4) {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Database error"
                                                                message:@"A server error has occured, please try again. If the problem persists, please contact an administrator"
                                                               delegate:nil
                                                      cancelButtonTitle:@"OK"
                                                      otherButtonTitles:nil];
                [alert show];
            }
            //deal with responses here
        }
        else {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Server error"
                                                            message:@"A server error has occured, please try again"
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
        }
    }
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    // The request has failed for some reason!
    // Check the error var
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Login error"
                                                    message:[error localizedDescription]
                                                   delegate:nil
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];
}




-(BOOL) NSStringIsValidEmail:(NSString *)checkString
{
    //BOOL stricterFilter = YES;
    NSString *stricterFilterString = @"[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}";
    //NSString *laxString = @".+@([A-Za-z0-9]+\\.)+[A-Za-z]{2}[A-Za-z]*";
    //NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", stricterFilterString];
    return [emailTest evaluateWithObject:checkString];
}

@end
