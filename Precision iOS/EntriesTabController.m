//
//  EntriesTabController.m
//  Precision iOS
//
//  Created by Zach Kauffman on 6/13/14.
//  Copyright (c) 2014 Zach Kauffman. All rights reserved.
//

#import "EntriesTabController.h"

@interface EntriesTabController ()

@end

@implementation EntriesTabController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    UITabBarController *tabBarController = self;
    UITabBar *tabBar = tabBarController.tabBar;
    
    UITabBarItem *firstTab = [tabBar.items objectAtIndex:0];
    
    // also repeat for every tab
    firstTab.image = [[UIImage imageNamed:@"check.png"] imageWithRenderingMode:UIImageResizingModeStretch|UIImageRenderingModeAlwaysTemplate];
    firstTab.selectedImage = [[UIImage imageNamed:@"check.png"]imageWithRenderingMode:UIImageResizingModeStretch|UIImageRenderingModeAlwaysOriginal];
    // repeat for every tab, but increment the index each time
    UITabBarItem *secondTab = [tabBar.items objectAtIndex:1];
    
    // also repeat for every tab
    secondTab.image = [[UIImage imageNamed:@"incomplete.png"] imageWithRenderingMode:UIImageResizingModeStretch|UIImageRenderingModeAlwaysTemplate];
    secondTab.selectedImage = [[UIImage imageNamed:@"incomplete.png"]imageWithRenderingMode:UIImageResizingModeStretch|UIImageRenderingModeAlwaysOriginal];
    
    UITabBarItem *thirdTab = [tabBar.items objectAtIndex:2];
    
    // also repeat for every tab
    thirdTab.image = [[UIImage imageNamed:@"email.png"] imageWithRenderingMode:UIImageResizingModeStretch|UIImageRenderingModeAlwaysTemplate];
    thirdTab.selectedImage = [[UIImage imageNamed:@"email.png"]imageWithRenderingMode:UIImageResizingModeStretch|UIImageRenderingModeAlwaysOriginal];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
