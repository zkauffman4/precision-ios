//
//  NewEntryViewController.m
//  Precision iOS
//
//  Created by Zach Kauffman on 5/30/14.
//  Copyright (c) 2014 Zach Kauffman. All rights reserved.
//

#import "NewEntryViewController.h"
#import "IncompleteEntryViewController.h"
#import "HomeTableViewController.h"
#import "AppDelegate.h"
#import "ViewController.h"
#import "StopWatchViewController.h"

@interface NewEntryViewController ()
@property (strong, nonatomic) NSArray *countryObjects;
@property (assign) BOOL testWithAutoCompleteObjectsInsteadOfStrings;


//Set this to true to prevent auto complete terms from returning instantly.
@property (assign) BOOL simulateLatency;
@end

@implementation NewEntryViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(IBAction)closeDateKeyboard
{
    [self.dateTextField resignFirstResponder];
}

-(IBAction)closeHoursKeyboard
{
    [self.hoursField resignFirstResponder];
}

-(IBAction)closeMinutesKeyboard
{
    [self.dateTextField resignFirstResponder];
}

-(IBAction)closeDescKeyboard
{
    [self.descriptionText resignFirstResponder];
}

-(IBAction)cancelBtnPressed:(id)sender
{
    [self.navigationController popViewControllerAnimated:TRUE];
}
-(IBAction)enterBtnPressed:(id)sender
{
    NSString *matter = _matterSearchBtn.titleLabel.text;
    NSString *date = _dateTextField.text;
    NSString *hoursWorked = _hoursField.text;
    NSString *description = _descriptionText.text;
    description = [description stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    BOOL switchVal = _switchField.isOn;
    NSString *task = _taskCodeBtn.titleLabel.text;
    NSString *comp = _componentCodeBtn.titleLabel.text;
    NSString *later = @"inactive";
    if(switchVal) {
        later = @"active";
    }
    if(_taskView.hidden) {
        task = @"";
        comp = @"";
    }
    if([task isEqualToString:@"Task Code"]) {
        task = @"";
    }
    
    if([comp isEqualToString:@"Component code"]) {
        comp = @"";
    }
    
    NSError *error;
    
    NSDataDetector *detector = [NSDataDetector dataDetectorWithTypes:NSTextCheckingTypeDate error:&error];
    
    NSUInteger valid = [detector numberOfMatchesInString:date options:0 range:NSMakeRange(0, date.length)];
    
    
    if(matter.length == 0) {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Incomplete entry"
                                                       message:@"Matter field is blank. Save this entry to incomplete list?"
                                                      delegate:self
                                             cancelButtonTitle:@"No"
                                             otherButtonTitles:@"Yes",nil];
        [alert show];
    } else if([hoursWorked isEqualToString:@"0:00"] && ![[NSUserDefaults standardUserDefaults] boolForKey:@"zeroTime"]) {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Incomplete entry"
                                                       message:@"Time worked is zero. Save this entry to incomplete list?"
                                                      delegate:self
                                             cancelButtonTitle:@"No"
                                             otherButtonTitles:@"Yes",nil];
        [alert show];
    } else if(description.length == 0) {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Incomplete entry"
                                                       message:@"Description field is blank. Save this entry to incomplete list?"
                                                      delegate:self
                                             cancelButtonTitle:@"No"
                                             otherButtonTitles:@"Yes",nil];
        [alert show];
    }
    else if(valid < 1) {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Incomplete entry"
                                                       message:@"Date field is invalid. Save this entry to incomplete list?"
                                                      delegate:self
                                             cancelButtonTitle:@"No"
                                             otherButtonTitles:@"Yes",nil];
        [alert show];
    } 
    
     else if(!_taskView.hidden && ((task == nil || [task isEqualToString:@""]) || (_componentCodeBtn.enabled && (comp == nil || [comp isEqualToString:@""])))) {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Incomplete entry"
                                                       message:@"This matter requires one or more task/component codes. Save this entry to incomplete list?"
                                                      delegate:self
                                             cancelButtonTitle:@"No"
                                             otherButtonTitles:@"Yes",nil];
        [alert show];
    } else {
        NSArray *timeParts = [hoursWorked componentsSeparatedByString:@":"];
        NSString *hourPart = [timeParts objectAtIndex:0];
        NSString *minutePart = [timeParts objectAtIndex:1];
        if([minutePart isEqualToString:@"00"]) {
            minutePart = @"0";
        }
        if(_editingEntry == nil) {
            NSString *url = [NSString stringWithFormat: @"https://precisiontimeentry.com/api/new_entry.php"];
            NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]];
            [request setHTTPMethod:@"POST"];
            NSString *postString = [NSString stringWithFormat:@"matter=%@&date=%@&hours=%@&minutes=%@&description=%@&lcode=%@&acode=%@&later=%@", matter, date, hourPart, minutePart, description,
                                    task, comp, later];
            [request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
            
            // Create url connection and fire request
            NSURLConnection *conn = [[NSURLConnection alloc] initWithRequest:request delegate:self];
            [_spinner startAnimating];
        } else {
            NSString *url = [NSString stringWithFormat: @"https://precisiontimeentry.com/api/update_entry.php"];
            NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]];
            [request setHTTPMethod:@"POST"];
            NSString *postString = [NSString stringWithFormat:@"entry=%@&matter=%@&date=%@&hours=%@&minutes=%@&description=%@&lcode=%@&acode=%@&later=%@&te=%@",_editingEntry.entryId, matter, date, hourPart, minutePart, description,
                                    task, comp, later, _te];
            [request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
            
            // Create url connection and fire request
            NSURLConnection *conn = [[NSURLConnection alloc] initWithRequest:request delegate:self];
            [_spinner startAnimating];
        }
    }
}

-(void) setEditingEntry:(EntryObject *)editingEntry
{
    _editingEntry = editingEntry;
    if(_editingEntry.lcode != nil && _editingEntry.lcode != [NSNull null] && ![_editingEntry.lcode isEqualToString:@"Task Code"] && ![_editingEntry.lcode isEqualToString:@""]) {
        NSString *url = [NSString stringWithFormat: @"https://precisiontimeentry.com/api/get_code_descriptions.php"];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]];
        [request setHTTPMethod:@"POST"];
        NSString *postString = [NSString stringWithFormat:@"lcode_id=%@&lcode_group=%@&acode_id=%@&acode_group=%@", _editingEntry.lcode, _editingEntry.lcodeGroup, _editingEntry.acode, _editingEntry.acodeGroup];
        NSLog(postString);
        [request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
        
        // Create url connection and fire request
        NSURLConnection *conn = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    }

}

-(void) setCloningEntry:(EntryObject *)cloningEntry
{
    _editingEntry = cloningEntry;
    if(_editingEntry.lcode != nil && _editingEntry.lcode != [NSNull null] && ![_editingEntry.lcode isEqualToString:@"Task Code"] && ![_editingEntry.lcode isEqualToString:@""]) {
        NSString *url = [NSString stringWithFormat: @"https://precisiontimeentry.com/api/get_code_descriptions.php"];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]];
        [request setHTTPMethod:@"POST"];
        NSString *postString = [NSString stringWithFormat:@"lcode_id=%@&lcode_group=%@&acode_id=%@&acode_group=%@", _editingEntry.lcode, _editingEntry.lcodeGroup, _editingEntry.acode, _editingEntry.acodeGroup];
        NSLog(postString);
        [request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
        
        // Create url connection and fire request
        NSURLConnection *conn = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    }
    _cloning = YES;
}

-(void) updateLabelsForEdit
{
    
    if(!_cloning) {
        self.parentViewController.title = @"Editing entry";
    }
    NSString *hrs = @"0:00";
    @try {
        NSArray *timeParts = [_editingEntry.hours componentsSeparatedByString:@"."];
        NSString *hourPart = [timeParts objectAtIndex:0];
        NSString *minutePart = [timeParts objectAtIndex:1];
        NSString *actualMinutes = [NSString stringWithFormat:@"%.f", [minutePart floatValue] * .6];
        if([actualMinutes isEqualToString:@"0"]) {
            actualMinutes = @"00";
        } else if([actualMinutes isEqualToString:@"6"]) {
            actualMinutes = @"06";
        }
        hrs = [NSString stringWithFormat:@"%@:%@", hourPart, actualMinutes];
        [_hoursField setText:hrs];
    }@catch(NSException *e) {
        [_hoursField setText:@"0:00"];
    }
    NSString *mName = @"Matter search";
    if(_editingEntry.matterName != (id)[NSNull null] && _editingEntry.matterName != nil) {
        [_matterSearchBtn setTitle:_editingEntry.matterId forState:UIControlStateNormal];
        [_matterSearchLabel setText:_editingEntry.matterName];
        
        NSString *url = [NSString stringWithFormat: @"https://precisiontimeentry.com/api/check_code_groups.php"];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]];
        [request setHTTPMethod:@"POST"];
        NSString *postString = [NSString stringWithFormat:@"matter_id=%@", _editingEntry.matterId];
        NSLog(postString);
        [request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
        
        // Create url connection and fire request
        NSURLConnection *conn = [[NSURLConnection alloc] initWithRequest:request delegate:self];

        
    }
    
    if(_editingEntry.date != (id)[NSNull null] && _editingEntry.date != nil) {
        [_dateTextField setText:_editingEntry.date];
    }
    
    if(_editingEntry.description != (id)[NSNull null] && _editingEntry.description != nil) {
        [_descriptionText setText:_editingEntry.description];
    }

    if(_editingEntry.lcode != nil && _editingEntry.lcode != (NSString*)[NSNull null] && ![_editingEntry.lcode isEqualToString:@""]) {
        if(_editingEntry.acode != nil && _editingEntry.acode != (NSString*)[NSNull null]) {
            [_componentCodeBtn setTitle:_editingEntry.acode forState:UIControlStateNormal];
        }
        [_taskCodeBtn setTitle:_editingEntry.lcode forState:UIControlStateNormal];
        [self showTaskView];
    } else {
        [self hideTaskView];
    }
    if(!_cloning) {
        self.navigationItem.title = @"Editing Entry";
    }
    
}

-(void)setTimerValue:(NSString *)str
{
    [_hoursField setText:str];
    [self.navigationController.presentedViewController dismissViewControllerAnimated:YES completion:nil];
    NSString *url = @"https://precisiontimeentry.com/api/refresh_session.php";
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:url]];
    NSURLConnection *conn = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    
}

-(IBAction)stopWatchPressed:(id)sender
{
    AppDelegate *appDelegate;
    appDelegate=(AppDelegate *)[[UIApplication sharedApplication] delegate];//in didLoad method
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:appDelegate.storyboardName bundle:nil];
    StopWatchViewController *v = [storyboard instantiateViewControllerWithIdentifier:@"stopWatchViewController"];
    [self presentViewController:v animated:YES completion:nil];
}

- (void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    // we should save to incomplete list
	if (buttonIndex == 1)
	{
        NSString *matter = _matterSearchBtn.titleLabel.text;
        NSString *date = _dateTextField.text;
        NSString *hoursWorked = _hoursField.text;
        NSString *task = _taskCodeBtn.titleLabel.text;
        NSString *component = _componentCodeBtn.titleLabel.text;
        NSString *description = _descriptionText.text;
        BOOL switchVal = _switchField.isOn;
        NSString *later = @"inactive";
        if(switchVal) {
            later = @"active";
        }
        
        if([matter isEqualToString:@"Matter Search"]) {
            matter = @"";
        }
        
        if(_taskView.hidden) {
            task = @"";
            component = @"";
        }
        if([task isEqualToString:@"Task Code"]) {
            task = @"";
        }
        
        if([component isEqualToString:@"Component code"]) {
            component = @"";
        }
        
        NSArray *timeParts = [hoursWorked componentsSeparatedByString:@":"];
        NSString *hourPart = [timeParts objectAtIndex:0];
        NSString *minutePart = [timeParts objectAtIndex:1];
        if([minutePart isEqualToString:@"00"]) {
            minutePart = @"0";
        }
	
        //its a new, incomplete entry
        if(_editingEntry == nil) {
            NSString *url = [NSString stringWithFormat: @"https://precisiontimeentry.com/api/new_entry.php"];
            NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]];
            [request setHTTPMethod:@"POST"];
            NSString *postString = [NSString stringWithFormat:@"matter=%@&date=%@&hours=%@&minutes=%@&description=%@&lcode=%@&acode=%@&later=%@&incomplete=1", matter, date, hourPart, minutePart, description, task, component, later];
            [request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
            
            // Create url connection and fire request
            NSURLConnection *conn = [[NSURLConnection alloc] initWithRequest:request delegate:self];
            [_spinner startAnimating];
        } else {    //its an updated, incomplete entry
            NSString *url = [NSString stringWithFormat: @"https://precisiontimeentry.com/api/update_entry.php"];
            NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]];
            [request setHTTPMethod:@"POST"];
            NSString *postString = [NSString stringWithFormat:@"entry=%@&matter=%@&date=%@&hours=%@&minutes=%@&description=%@&lcode=%@&acode=%@&later=%@&te=%@&incomplete=1",_editingEntry.entryId, matter, date, hourPart, minutePart, description, task, component, later, _te];
            [request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
            NSLog(postString);
            // Create url connection and fire request
            NSURLConnection *conn = [[NSURLConnection alloc] initWithRequest:request delegate:self];
            [_spinner startAnimating];
        }
    }
}

-(IBAction)matterSearchBtnPressed:(id)sender
{
    AppDelegate *appDelegate;
    appDelegate=(AppDelegate *)[[UIApplication sharedApplication] delegate];//in didLoad method
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:appDelegate.storyboardName bundle:nil];
    MatterSearchViewController *v = [storyboard instantiateViewControllerWithIdentifier:@"matterSearchView"];
    v.delegate = self;
    [self.navigationController pushViewController:v animated:YES];
}

-(IBAction)taskCodeSearchBtnPressed:(id)sender
{
    AppDelegate *appDelegate;
    appDelegate=(AppDelegate *)[[UIApplication sharedApplication] delegate];//in didLoad method
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:appDelegate.storyboardName bundle:nil];
    TaskCodeViewController *v = [storyboard instantiateViewControllerWithIdentifier:@"taskCodeSearch"];
    v.delegate = self;
    [v setcodeGroup:_selectedMatter.tGroup];
    [self.navigationController pushViewController:v animated:YES];
}

-(IBAction)componentCodeSearchBtnPressed:(id)sender
{
    AppDelegate *appDelegate;
    appDelegate=(AppDelegate *)[[UIApplication sharedApplication] delegate];//in didLoad method
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:appDelegate.storyboardName bundle:nil];
    ComponentSearchViewController *v = [storyboard instantiateViewControllerWithIdentifier:@"componentCodeSearch"];
    v.delegate = self;
    [v setcodeGroup:_selectedMatter.cGroup];
    [self.navigationController pushViewController:v animated:YES];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    _isIncomplete = NO;
    _taskStartingY = [_taskView frame].origin.y;
    _submitStartingY = [_submitView frame].origin.y;
    
    _spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    _spinner.opaque = NO;
    _spinner.backgroundColor = [UIColor colorWithWhite:0.0f alpha:0.6f];
    [_spinner setBounds:self.view.bounds];
    _spinner.center = self.view.center;
    _spinner.activityIndicatorViewStyle = UIActivityIndicatorViewStyleGray;
    [_spinner setColor:[UIColor colorWithRed:0.6 green:0.8 blue:1.0 alpha:1.0]];
    [self.view addSubview:_spinner]; // spinner is not visible until started
    
    [self hideTaskView];
    self.navigationItem.title = @"New Entry";
    
    self.searchDisplayController.delegate = self;
    self.descriptionText.delegate = self;
    UIToolbar *descToolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, 44)];
    
    UIBarButtonItem *s = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    
    UIBarButtonItem *d = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(closeDescKeyboard)];
    
    [descToolbar setItems:[[NSArray alloc] initWithObjects: s, d, nil]];
    [[self.descriptionText layer] setBorderColor:[[UIColor grayColor] CGColor]];
    [[self.descriptionText layer] setBorderWidth:.4];
    [[self.descriptionText layer] setCornerRadius:10];
    self.descriptionText.inputAccessoryView = descToolbar;
    _titleArray =  [[NSArray alloc] initWithObjects:@"Hours",@"Minutes",nil];
    _hoursArray =  [[NSArray alloc] initWithObjects:@"0",@"1",@"2",@"3",@"4",@"5",@"6",@"7",@"8",nil];
    _minutesArray =  [[NSArray alloc] initWithObjects:@"00",@"06",@"12",@"15",@"18",@"24",@"30",@"36",@"42",@"45",@"48",@"54",nil];
    //Date picker
    NSDateFormatter *formatter;
    NSString        *dateString;
    
    formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"MM/dd/YYYY"];
    
    dateString = [formatter stringFromDate:[NSDate date]];
    if (_keyboardToolbar == nil) {
        _keyboardToolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, 44)];
        
        UIBarButtonItem *extraSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
        
        UIBarButtonItem *accept = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(closeDateKeyboard)];
        
        [_keyboardToolbar setItems:[[NSArray alloc] initWithObjects: extraSpace, accept, nil]];
    }
    self.dateTextField.text = dateString;
    self.dateTextField.inputAccessoryView = _keyboardToolbar;
    
    _datePicker = [[UIDatePicker alloc] init];
    _datePicker.datePickerMode = UIDatePickerModeDate;
    
    [_datePicker addTarget:self action:@selector(datePickerValueChanged:) forControlEvents:UIControlEventValueChanged];
    [_datePicker setBackgroundColor:[UIColor whiteColor]];
    self.dateTextField.inputView = _datePicker;
    
    //hour picker
    _myPickerView = [[UIPickerView alloc] init];
    [_myPickerView setBackgroundColor:[UIColor whiteColor]];
    //myPickerView configuration here...
    _hoursField.inputView = _myPickerView;
    UIToolbar *myToolbar = [[UIToolbar alloc] initWithFrame:
                            CGRectMake(0,0, self.view.bounds.size.width, 44)]; //should code with variables to support view resizing
    UIBarButtonItem *extraSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    UIBarButtonItem *doneButton =
    [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone
                                                  target:self action:@selector(closeHoursKeyboard)];
    //using default text field delegate method here, here you could call
    //myTextField.resignFirstResponder to dismiss the views
    [myToolbar setItems:[[NSArray alloc ]initWithObjects: extraSpace, doneButton, nil] animated:NO];
    _hoursField.inputAccessoryView = myToolbar;
    _myPickerView.delegate = self;
    _myPickerView.dataSource = self;

    NSString *strDay = @"Hours";
    float lblWidth = _myPickerView.frame.size.width / _myPickerView.numberOfComponents;
    float lblXposition = _myPickerView.frame.origin.x;
    float lblYposition = (_myPickerView.frame.origin.y + 10);
    
    UILabel *lblDay = [[UILabel alloc] initWithFrame:CGRectMake(lblXposition,
                                                                lblYposition,
                                                                lblWidth,
                                                                20)];
    [lblDay setText:strDay];
    [lblDay setTextAlignment:NSTextAlignmentCenter];
    
    [_myPickerView addSubview:lblDay];
    
    NSString *strMinutes = @"Minutes";
    float lbl2Width = _myPickerView.frame.size.width /_myPickerView.numberOfComponents;
    float lbl2Xposition = _myPickerView.frame.origin.x + lbl2Width;
    float lbl2Yposition = (_myPickerView.frame.origin.y + 10);
    
    UILabel *lblMin = [[UILabel alloc] initWithFrame:CGRectMake(lbl2Xposition,
                                                                lbl2Yposition,
                                                                lbl2Width,
                                                                20)];
    [lblMin setText:strMinutes];
    [lblMin setTextAlignment:NSTextAlignmentCenter];
    
    [_myPickerView addSubview:lblMin];
    _hoursField.text = @"0:00";
    _matterSearchBtn.layer.cornerRadius = 5.0f;
    _componentCodeBtn.layer.cornerRadius = 5.0f;
    _taskCodeBtn.layer.cornerRadius = 5.0f;
    if(_editingEntry != nil) {
        [self updateLabelsForEdit];
    }
    if(_cloning) {
        _editingEntry = nil;
    }
    
}

-(void)showTaskView
{
    _submitView.frame = CGRectMake([_submitView frame].origin.x, _submitStartingY, [_submitView frame].size.width, [_submitView frame].size.height);
    [_taskView setHidden:NO];
}

-(void)hideTaskView
{
    [_taskView setHidden:YES];
    _submitView.frame = CGRectMake([_submitView frame].origin.x, _taskStartingY, [_submitView frame].size.width, [_submitView frame].size.height);
}



-(BOOL)textViewShouldEndEditing:(UITextView *)textView
{
    [self scrollBack];
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    [self scrollBack];
    return YES;
}

- (BOOL)textViewShouldBeginEditing:(UITextView *)textField{
    
    [self scrollViewToCenterOfScreen:textField];
    return YES;
    
}

- (void)scrollViewToCenterOfScreen:(UIView *)theView {
    CGFloat viewCenterY = theView.center.y;
    CGRect applicationFrame = [[UIScreen mainScreen] applicationFrame];
    
    CGFloat availableHeight = applicationFrame.size.height - 200;            // Remove area covered by keyboard
    
    CGFloat y = viewCenterY - availableHeight / 2.0;
    if (y < 0) {
        y = 0;
    }
    _isScrolled = YES;
    
    [self.scrollView setContentOffset:CGPointMake(0, y) animated:YES];
    
}

- (void) scrollBack
{
    if ( ! _isScrolled ) return;

    [self.scrollView setContentOffset:CGPointMake(0, 0) animated:YES];
    _isScrolled = NO;
}

-(void)viewDidAppear:(BOOL)animated
{
    CGSize scrollViewContentSize = CGSizeMake(284, 686);
    [self.scrollView setContentSize:scrollViewContentSize];
    _scrollView.delegate = self;
    _scrollView.scrollEnabled = YES;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    
    NSInteger row1 = [_myPickerView selectedRowInComponent:0];
    NSInteger row2 = [_myPickerView selectedRowInComponent:1];
    
    _selectedHour = [_hoursArray objectAtIndex:row1];
    _selectedMinute = [_minutesArray objectAtIndex:row2];
    
    NSString *currentTime = [NSString stringWithFormat:@"%@:%@", _selectedHour, _selectedMinute];
    _hoursField.text = currentTime;
    
}

- (void)datePickerValueChanged:(id)sender{
    
    _date = _datePicker.date;
    
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"MM/dd/YYYY"];
    
    
    [self.dateTextField setText:[df stringFromDate:_date]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 2; // For one column
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    if(component == 0) {
        return [_hoursArray count];
    } else if(component == 1) {
        return [_minutesArray count];
    }
    return 0; // Numbers of rows
}

-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    if(component == 0) {
        return [_hoursArray objectAtIndex:row];
    } else {
        return [_minutesArray objectAtIndex:row];
    }
}

#pragma mark NSURLConnection Delegate Methods

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    // A response has been received, this is where we initialize the instance var you created
    // so that we can append data to it in the didReceiveData method
    // Furthermore, this method is called each time there is a redirect so reinitializing it
    // also serves to clear it
    _responseData = [[NSMutableData alloc] init];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    // Append the new data to the instance variable you declared
    [_responseData appendData:data];
}

- (NSCachedURLResponse *)connection:(NSURLConnection *)connection
                  willCacheResponse:(NSCachedURLResponse*)cachedResponse {
    // Return nil to indicate not necessary to store a cached response for this connection
    return nil;
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    // The request is complete and data has been received
    // You can parse the stuff in your instance variable now
    [_spinner stopAnimating];
    NSError *jsonParsingError = nil;
    id object = [NSJSONSerialization JSONObjectWithData:_responseData options:0 error:&jsonParsingError];
    
    if (jsonParsingError) {
        NSLog(@"JSON ERROR: %@", [jsonParsingError localizedDescription]);
    }
    else
    {
        if([object isKindOfClass:[NSDictionary class]])
        {
            //trying to delete?
            NSDictionary *results = object;
            //NSLog(@"Data: %@", [results description]);
            if([results objectForKey:@"success"] != nil)
            {
                [self.navigationController popViewControllerAnimated:YES];
            }
            else if([results objectForKey:@"codes"] != nil)
            {
                NSArray *codes = [results objectForKey:@"codes"];
                NSLog([codes description]);
                
                NSDictionary *lcodeDesc = [codes objectAtIndex:0];
                [_tCodeLabel setText:[lcodeDesc objectForKey:@"code_desc"]];
                
                if([codes count] > 1)
                {
                    NSDictionary *acodeDesc = [codes objectAtIndex:1];
                    [_componentLabel setText:[acodeDesc objectForKey:@"code_desc"]];
                }
                else
                {
                    [_componentLabel setText:@"Tap to select component code"];
                }
                
            }
            else if([results objectForKey:@"error"] != nil)
            {
                NSString *error = [results objectForKey:@"error"];
                if(error != nil && error != (id)[NSNull null] && [error isEqualToString:@"invalid session"])
                {
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Session expiration"
                                                                    message:@"Your session has expired. As a security measure, you will be logged out."
                                                                   delegate:nil
                                                          cancelButtonTitle:@"OK"
                                                          otherButtonTitles:nil];
                    [alert show];
                }
                else
                {
                    [self.navigationController popToRootViewControllerAnimated:YES];
                }
            } else if([results objectForKey:@"num_codes"] != nil) {
                if([[results objectForKey:@"num_codes"] intValue] == 1) {
                    [self showTaskView];
                }
            } else if([results objectForKey:@"session"] != nil) {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Session Timeout"
                                                                message:@"You have had more than 15 minutes of inactivity. To protect your account, you have been logged out. Please log in again."
                                                               delegate:nil
                                                      cancelButtonTitle:@"OK"
                                                      otherButtonTitles:nil];
                [alert show];
                AppDelegate *appDelegate;
                appDelegate=(AppDelegate *)[[UIApplication sharedApplication] delegate];//in didLoad method
                UIStoryboard *storyboard = [UIStoryboard storyboardWithName:appDelegate.storyboardName bundle:nil];
                ViewController *v = [storyboard instantiateViewControllerWithIdentifier:@"loginController"];
                [self presentViewController:v animated:YES completion:nil];
            }

            else
            {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Database error"
                                                                message:@"A database error has occurred"
                                                               delegate:nil
                                                      cancelButtonTitle:@"OK"
                                                      otherButtonTitles:nil];
                [alert show];
            }
        }
    }
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    // The request has failed for some reason!
    // Check the error var
    [_spinner stopAnimating];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Connection error"
                                                    message:[error localizedDescription]
                                                   delegate:nil
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];
}

-(void)setComponent:(TaskCodeObject *)t
{
    _selectedComponent = t;
    [_componentCodeBtn setTitle:_selectedComponent.codeId forState:UIControlStateNormal];
    [_componentLabel setText:_selectedComponent.codeName];
}

-(void)setMatter: (MatterObject*) m
{
    _selectedMatter = m;
    _selectedTask = nil;
    _selectedComponent = nil;
    [_matterSearchBtn setTitle:_selectedMatter.matterId forState:UIControlStateNormal];
    [_matterSearchLabel setText:_selectedMatter.matterName];
    [_taskCodeBtn setTitle:@"Task Code" forState:UIControlStateNormal];
    [_tCodeLabel setText:@"Tap to select task code"];
    [_componentCodeBtn setTitle:@"Component code" forState:UIControlStateNormal];
    [_componentLabel setText:@"Tap to select component code"];
    
    if(_selectedMatter.tGroup != nil && _selectedMatter.tGroup != (id)[NSNull null]) {
        [self showTaskView];
        if(_selectedMatter.cGroup == nil || _selectedMatter.cGroup == (id)[NSNull null] || [_selectedMatter.cGroup isEqualToString:@""])
        {
            [_componentCodeBtn setEnabled:NO];
            [_componentCodeBtn setBackgroundColor:[UIColor grayColor]];
            [_cCodeArrow setHidden:YES];
        } else {
            [_componentCodeBtn setEnabled:YES];
            [_componentCodeBtn setBackgroundColor:[UIColor whiteColor]];
            [_cCodeArrow setHidden:NO];
        }
    } else {
        [self hideTaskView];
    }
}

-(void)setTask: (TaskCodeObject*)t
{
    _selectedTask = t;
    [_taskCodeBtn setTitle:_selectedTask.codeId forState:UIControlStateNormal];
    [_tCodeLabel setText:_selectedTask.codeName];
}



@end
