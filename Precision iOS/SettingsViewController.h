//
//  SettingsViewController.h
//  Precision iOS
//
//  Created by Zach Kauffman on 6/4/14.
//  Copyright (c) 2014 Zach Kauffman. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingsViewController : UIViewController<UITextFieldDelegate>
@property (strong, nonatomic) IBOutlet UISwitch *offlineEntrySwitch;
@property (strong, nonatomic) IBOutlet UISwitch *goalTrackingSwitch;
@property (strong, nonatomic) IBOutlet UISwitch *zeroTime;
@property (strong, nonatomic) IBOutlet UISlider *weeklyGoalSlider;
@property (strong, nonatomic) IBOutlet UISwitch *enabledEmailLogs;
@property (strong, nonatomic) IBOutlet UILabel *weeklyGoalLabel;
@property (strong, nonatomic) IBOutlet UITextField *emailLogsUser;
@property (strong, nonatomic) IBOutlet UITextField *emailLogsText;
@property (strong, nonatomic) IBOutlet UITextField *exchangeEmailTextfield;
@property (strong, nonatomic) IBOutlet UIButton *doneBtn;
@property (strong, nonatomic) NSMutableData *responseData;
@property (strong, nonatomic) UIActivityIndicatorView *spinner;
@property (strong, nonatomic) NSString *oldEmail;
-(IBAction)doneBtnPressed:(id)sender;
@end
