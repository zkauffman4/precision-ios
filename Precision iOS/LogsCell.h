//
//  LogsCell.h
//  Precision iOS
//
//  Created by Zach Kauffman on 6/13/14.
//  Copyright (c) 2014 Zach Kauffman. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LogsCell : UITableViewCell
@property (strong, nonatomic) UILabel *subjectLabel;
@property (strong, nonatomic) UILabel *emailLabel;
@property (strong, nonatomic) UILabel *dateLabel;
@end
