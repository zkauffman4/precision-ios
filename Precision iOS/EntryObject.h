//
//  EntryObject.h
//  Precision iOS
//
//  Created by Zach Kauffman on 5/29/14.
//  Copyright (c) 2014 Zach Kauffman. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface EntryObject : NSObject
-(id)initWithJSONData:(NSDictionary*)data;

@property (strong) NSString *entryId;
@property (strong) NSString *matterId;
@property (strong) NSString *matterName;
@property (strong) NSString *description;
@property (strong) NSString *lcode;
@property (strong) NSString *acode;
@property (strong) NSString *hours;
@property (strong) NSString *date;
@property (strong) NSString *lcodeGroup;
@property (strong) NSString *acodeGroup;
@end
