//
//  LogsCell.m
//  Precision iOS
//
//  Created by Zach Kauffman on 6/13/14.
//  Copyright (c) 2014 Zach Kauffman. All rights reserved.
//

#import "LogsCell.h"

@implementation LogsCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        _subjectLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, -32, self.contentView.frame.size.width, self.contentView.frame.size.height)];
        [self.contentView addSubview:_subjectLabel];
        [self.contentView sendSubviewToBack:_subjectLabel];
        _subjectLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:17];
        _emailLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, -10, self.contentView.frame.size.width/2, self.contentView.frame.size.height)];
        //_emailLabel.clipsToBounds = YES;
        //_emailLabel.layer.masksToBounds = YES;
        [self.contentView addSubview:_emailLabel];
        [self.contentView sendSubviewToBack:_emailLabel];
        _emailLabel.font = [UIFont fontWithName:@"Helvetica" size:14];
        _emailLabel.textColor = [UIColor grayColor];
        _dateLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 15, self.contentView.frame.size.width, self.contentView.frame.size.height)];
        [self.contentView addSubview:_dateLabel];
        [self.contentView sendSubviewToBack:_dateLabel];
        _dateLabel.font = [UIFont fontWithName:@"Helvetica" size:14];
        _dateLabel.textColor = [UIColor grayColor];

    }
    return self;
}

-(id) initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        _subjectLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, -32, self.contentView.frame.size.width, self.contentView.frame.size.height)];
        [self.contentView addSubview:_subjectLabel];
        [self.contentView sendSubviewToBack:_subjectLabel];
        _subjectLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:17];
        _emailLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, self.contentView.frame.size.width, self.contentView.frame.size.height)];
        [self.contentView addSubview:_emailLabel];
        [self.contentView sendSubviewToBack:_emailLabel];
        _emailLabel.font = [UIFont fontWithName:@"Helvetica" size:14];
        _emailLabel.textColor = [UIColor grayColor];
        _dateLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 32, self.contentView.frame.size.width, self.contentView.frame.size.height)];
        [self.contentView addSubview:_dateLabel];
        [self.contentView sendSubviewToBack:_dateLabel];
        _dateLabel.font = [UIFont fontWithName:@"Helvetica" size:14];
        _dateLabel.textColor = [UIColor grayColor];
        
    }
    return self;

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


@end
