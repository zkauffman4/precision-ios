//
//  GraphViewController.h
//  Precision iOS
//
//  Created by Zach Kauffman on 6/5/14.
//  Copyright (c) 2014 Zach Kauffman. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PNChart.h"

@interface GraphViewController : UITableViewController<PNChartDelegate>

@property (nonatomic) PNBarChart * barChart;
@property (strong, nonatomic) NSMutableData *responseData;
@property (strong, nonatomic) NSString *period;
@property (strong, nonatomic) NSString *type;
@property (strong, nonatomic) UIActivityIndicatorView *spinner;
@end
