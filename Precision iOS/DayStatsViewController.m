//
//  DayStatsViewController.m
//  Precision iOS
//
//  Created by Zach Kauffman on 6/4/14.
//  Copyright (c) 2014 Zach Kauffman. All rights reserved.
//

#import "DayStatsViewController.h"
#import "HoursByMatterViewController.h"
#import "GraphViewController.h"
#import "AppDelegate.h"
#import "ViewController.h"


@interface DayStatsViewController ()

@end

@implementation DayStatsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void) viewDidAppear:(BOOL)animated
{
    self.parentViewController.title = @"Weekly Stats";
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    CGAffineTransform transform = CGAffineTransformMakeScale(1.0f, 5.0f);
    _progressBar.transform = transform;
    
    NSString *url = [NSString stringWithFormat: @"https://precisiontimeentry.com/api/get_time_stats.php?period=week"];
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:url]];
    
    // Create url connection and fire request
    NSURLConnection *conn = [[NSURLConnection alloc] initWithRequest:request delegate:self];

    _spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    _spinner.opaque = NO;
    _spinner.backgroundColor = [UIColor colorWithWhite:0.0f alpha:0.6f];
    [_spinner setBounds:self.view.bounds];
    _spinner.center = self.view.center;
    _spinner.activityIndicatorViewStyle = UIActivityIndicatorViewStyleGray;
    [_spinner setColor:[UIColor colorWithRed:0.6 green:0.8 blue:1.0 alpha:1.0]];
    [self.view addSubview:_spinner]; // spinner is not visible until started
    [_spinner startAnimating];
    _hours.layer.cornerRadius = 5.0f;
    _entries.layer.cornerRadius = 5.0f;
    _matters.layer.cornerRadius = 5.0f;
    _goal.layer.cornerRadius = 5.0f;
    int currentHours = 28;
    
    BOOL isGoalProgressEnabled = [[NSUserDefaults standardUserDefaults] boolForKey:@"weeklyTracking"];
    if(!isGoalProgressEnabled) {
        [_goal setHidden:YES];
    }
    
    
	// Do any additional setup after loading the view.

    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(imageTapDetected)];
    singleTap.numberOfTapsRequired = 1;
    
    UITapGestureRecognizer *singleTap2 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(imageTapDetected)];
    singleTap2.numberOfTapsRequired = 1;
    
    _hoursByMatterLabel.userInteractionEnabled = YES;
    [_hoursByMatterLabel addGestureRecognizer:singleTap];
    
    _hoursByMatterImage.userInteractionEnabled = YES;
    [_hoursByMatterImage addGestureRecognizer:singleTap2];
    
    UITapGestureRecognizer *entriesTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(entriesTapDetected)];
    entriesTap.numberOfTapsRequired = 1;
    
    UITapGestureRecognizer *entriesTap2 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(entriesTapDetected)];
    entriesTap2.numberOfTapsRequired = 1;
    
    _entriesByMatterImage.userInteractionEnabled = YES;
    [_entriesByMatterImage addGestureRecognizer:entriesTap];
    
    _entriesByMatterLabel.userInteractionEnabled = YES;
    [_entriesByMatterLabel addGestureRecognizer:entriesTap2];
    
    UITapGestureRecognizer *hoursByDayTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hoursByDayPressed)];
    hoursByDayTap.numberOfTapsRequired = 1;
    
    UITapGestureRecognizer *hoursByDayTap2 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hoursByDayPressed)];
    hoursByDayTap2.numberOfTapsRequired = 1;
    
    _hoursByDayImage.userInteractionEnabled = YES;
    [_hoursByDayImage addGestureRecognizer:hoursByDayTap];
    
    _hoursByDayLabel.userInteractionEnabled = YES;
    [_hoursByDayLabel addGestureRecognizer:hoursByDayTap2];
    
    UITapGestureRecognizer *entriesByDayTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(entriesByDayPressed)];
    entriesByDayTap.numberOfTapsRequired = 1;
    
    UITapGestureRecognizer *entriesByDayTap2 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(entriesByDayPressed)];
    entriesByDayTap2.numberOfTapsRequired = 1;
    
    _entriesByDayImage.userInteractionEnabled = YES;
    [_entriesByDayImage addGestureRecognizer:entriesByDayTap];
    
    _entriesByDayLabel.userInteractionEnabled = YES;
    [_entriesByDayLabel addGestureRecognizer:entriesByDayTap2];
    
    UITapGestureRecognizer *mattersByDayTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(mattersByDayPressed)];
    mattersByDayTap.numberOfTapsRequired = 1;
    
    UITapGestureRecognizer *mattersByDayTap2 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(mattersByDayPressed)];
    mattersByDayTap2.numberOfTapsRequired = 1;
    
    _mattersByDayImage.userInteractionEnabled = YES;
    [_mattersByDayImage addGestureRecognizer:mattersByDayTap];
    
    _mattersByDayLabel.userInteractionEnabled = YES;
    [_mattersByDayLabel addGestureRecognizer:mattersByDayTap2];
    
    

    
    
    
}
-(void)imageTapDetected{
    AppDelegate *appDelegate;
    appDelegate=(AppDelegate *)[[UIApplication sharedApplication] delegate];//in didLoad method
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:appDelegate.storyboardName bundle:nil];
    HoursByMatterViewController *v = [storyboard instantiateViewControllerWithIdentifier:@"hoursByMatter"];
    v.period = @"week";
    [self.navigationController pushViewController:v animated:YES];
    
}

-(void)entriesTapDetected{
    AppDelegate *appDelegate;
    appDelegate=(AppDelegate *)[[UIApplication sharedApplication] delegate];//in didLoad method
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:appDelegate.storyboardName bundle:nil];
    HoursByMatterViewController *v = [storyboard instantiateViewControllerWithIdentifier:@"entriesByMatter"];
    v.period = @"week";
    [self.navigationController pushViewController:v animated:YES];
    
}

-(void) hoursByDayPressed
{
    AppDelegate *appDelegate;
    appDelegate=(AppDelegate *)[[UIApplication sharedApplication] delegate];//in didLoad method
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:appDelegate.storyboardName bundle:nil];
    GraphViewController *v = [storyboard instantiateViewControllerWithIdentifier:@"graphView"];
    v.period = @"day";
    v.type = @"hours";
    
    [self.navigationController pushViewController:v animated:YES];
}

-(void) entriesByDayPressed
{
    AppDelegate *appDelegate;
    appDelegate=(AppDelegate *)[[UIApplication sharedApplication] delegate];//in didLoad method
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:appDelegate.storyboardName bundle:nil];
    GraphViewController *v = [storyboard instantiateViewControllerWithIdentifier:@"graphView"];
    v.period = @"day";
    v.type = @"entries";
    [self.navigationController pushViewController:v animated:YES];
}

-(void) mattersByDayPressed
{
    AppDelegate *appDelegate;
    appDelegate=(AppDelegate *)[[UIApplication sharedApplication] delegate];//in didLoad method
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:appDelegate.storyboardName bundle:nil];
    GraphViewController *v = [storyboard instantiateViewControllerWithIdentifier:@"graphView"];
    v.period = @"day";
    v.type = @"matters";
    [self.navigationController pushViewController:v animated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark NSURLConnection Delegate Methods

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
   
    _responseData = [[NSMutableData alloc] init];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    [_responseData appendData:data];
}

- (NSCachedURLResponse *)connection:(NSURLConnection *)connection
                  willCacheResponse:(NSCachedURLResponse*)cachedResponse {
    
    return nil;
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    [_spinner stopAnimating];
    NSError *jsonParsingError = nil;
    id object = [NSJSONSerialization JSONObjectWithData:_responseData options:0 error:&jsonParsingError];
    
    if (jsonParsingError) {
        NSLog(@"JSON ERROR: %@", [jsonParsingError localizedDescription]);
    } else {
        if([object isKindOfClass:[NSDictionary class]]) {
            //trying to delete?
            NSDictionary *results = object;
            if([results objectForKey:@"stats"] != nil) {
                NSDictionary *set = [results objectForKey:@"stats"];
                [_hoursLabel setText:[NSString stringWithFormat:@"Hours this week: %@", [set objectForKey:@"total_hours"]]];
                
                [_entriesLabel setText:[NSString stringWithFormat:@"Entries this week: %@", [set objectForKey:@"total_entries"]]];
                
                [_mattersLabel setText:[NSString stringWithFormat:@"Matters this week: %@", [set objectForKey:@"total_matters"]]];
                
                if([[NSUserDefaults standardUserDefaults] boolForKey:@"weeklyTracking"]) {
                    int maxHours = [[NSUserDefaults standardUserDefaults] integerForKey:@"weeklyGoalValue"];
                    NSString *hr =  [set objectForKey:@"total_hours"];
                    float total_hours = [hr floatValue];
                    float percentage = (float)total_hours/maxHours;
                    [_progressBar setProgress:percentage];
                    _goalProgressLabel.text = [NSString stringWithFormat:@"Weekly Goal Progress: %.f%%", percentage*100];
                }
            } else if([object objectForKey:@"session"] != nil) {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Session Timeout"
                                                                message:@"You have had more than 15 minutes of inactivity. To protect your account, you have been logged out. Please log in again."
                                                               delegate:nil
                                                      cancelButtonTitle:@"OK"
                                                      otherButtonTitles:nil];
                [alert show];
                AppDelegate *appDelegate;
                appDelegate=(AppDelegate *)[[UIApplication sharedApplication] delegate];//in didLoad method
                UIStoryboard *storyboard = [UIStoryboard storyboardWithName:appDelegate.storyboardName bundle:nil];
                ViewController *v = [storyboard instantiateViewControllerWithIdentifier:@"loginController"];
                [self presentViewController:v animated:YES completion:nil];
            }

        }else {
            NSString *d = [[NSString alloc] initWithData:_responseData encoding:NSUTF8StringEncoding];
        }
    }
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    [_spinner stopAnimating];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Login error"
                                                    message:[error localizedDescription]
                                                   delegate:nil
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];
}



@end
