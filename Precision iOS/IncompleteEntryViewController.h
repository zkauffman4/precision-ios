//
//  IncompleteEntryViewController.h
//  Precision iOS
//
//  Created by Zach Kauffman on 5/29/14.
//  Copyright (c) 2014 Zach Kauffman. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWTableViewCell.h"

@interface IncompleteEntryViewController : UITableViewController<SWTableViewCellDelegate>

@property (nonatomic, strong) NSMutableArray * incomplete;
@property (nonatomic, strong) NSMutableData *responseData;
@property (nonatomic, strong) NSMutableArray *entryArray;
@property (nonatomic, strong) NSMutableArray *selectedItems;
@property (nonatomic, strong) UIActivityIndicatorView *spinner;
-(void)refreshData;
@end

