//
//  HoursByMatterViewController.h
//  Precision iOS
//
//  Created by Zach Kauffman on 6/5/14.
//  Copyright (c) 2014 Zach Kauffman. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EntriesByMatterViewController : UITableViewController<UISearchBarDelegate, UISearchDisplayDelegate, UITableViewDataSource, UITableViewDelegate>

@property (strong, nonatomic) NSMutableArray* mattersArray;
@property (strong, nonatomic) NSMutableArray* filteredTableData;
@property (strong, nonatomic) NSMutableData *responseData;
@property BOOL isFiltered;
@property (strong, nonatomic) IBOutlet UISearchBar *searchBar;
@property (strong, nonatomic) NSString *period;
@property (strong, nonatomic) UIActivityIndicatorView *spinner;

@end
