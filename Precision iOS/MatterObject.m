//
//  MatterObject.m
//  Precision iOS
//
//  Created by Zach Kauffman on 6/1/14.
//  Copyright (c) 2014 Zach Kauffman. All rights reserved.
//

#import "MatterObject.h"

@implementation MatterObject

-(MatterObject*) initWithId:(NSString *)matterId andName:(NSString *)matterName taskCodeGroup:(NSString *)tGroup componentCodeGroup:(NSString *)cGroup
{
    self = [MatterObject alloc];
    self.matterName = matterName;
    self.matterId = matterId;
    self.tGroup = tGroup;
    self.cGroup = cGroup;
    return self;
}

@end
