//
//  ResetPasswordViewController.m
//  Precision iOS
//
//  Created by Zach Kauffman on 6/11/14.
//  Copyright (c) 2014 Zach Kauffman. All rights reserved.
//

#import "ResetPasswordViewController.h"

@interface ResetPasswordViewController ()

@end

@implementation ResetPasswordViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    _usernameTextfield.delegate = self;
    _emailTextfield.delegate = self;
    
    _usernameTextfield.borderStyle = UITextBorderStyleRoundedRect;
    _emailTextfield.borderStyle = UITextBorderStyleRoundedRect;
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)resetBtnPressed:(id)sender
{
    if([_usernameTextfield.text length] == 0 || [_emailTextfield.text length] == 0)
    {
        //need both fields
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Reset error"
                                                        message:@"Both fields must be entered in order to reset password"
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];

    } else {
        //post to reset password
        
        NSString *url = [NSString stringWithFormat: @"https://precisiontimeentry.com/api/reset_password.php"];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]];
        [request setHTTPMethod:@"POST"];
        NSString *postString = [NSString stringWithFormat:@"username=%@&email=%@", _usernameTextfield.text, _emailTextfield.text];
        [request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
        
        // Create url connection and fire request
        NSURLConnection *conn = [[NSURLConnection alloc] initWithRequest:request delegate:self];
        
        _spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        _spinner.opaque = NO;
        _spinner.backgroundColor = [UIColor colorWithWhite:0.0f alpha:0.6f];
        [_spinner setBounds:self.view.bounds];
        _spinner.center = self.view.center;
        _spinner.activityIndicatorViewStyle = UIActivityIndicatorViewStyleGray;
        [_spinner setColor:[UIColor colorWithRed:0.6 green:0.8 blue:1.0 alpha:1.0]];
        [self.view addSubview:_spinner]; // spinner is not visible until started
        [_spinner startAnimating];
    }
}

-(IBAction)loginBtnPressed:(id)sender
{
    [self performSegueWithIdentifier:@"resetToLogin" sender:self];
}

#pragma mark NSURLConnection Delegate Methods

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    // A response has been received, this is where we initialize the instance var you created
    // so that we can append data to it in the didReceiveData method
    // Furthermore, this method is called each time there is a redirect so reinitializing it
    // also serves to clear it
    _responseData = [[NSMutableData alloc] init];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    // Append the new data to the instance variable you declared
    [_responseData appendData:data];
}

- (NSCachedURLResponse *)connection:(NSURLConnection *)connection
                  willCacheResponse:(NSCachedURLResponse*)cachedResponse {
    // Return nil to indicate not necessary to store a cached response for this connection
    return nil;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    [self scrollBack];
    return YES;
}

- (BOOL)textFieldShouldBeginEditing:(UITextView *)textField{
    
    [self scrollViewToCenterOfScreen:textField];
    return YES;
    
}

- (void)scrollViewToCenterOfScreen:(UIView *)theView {
    CGFloat viewCenterY = theView.center.y;
    CGRect applicationFrame = [[UIScreen mainScreen] applicationFrame];
    
    CGFloat availableHeight = applicationFrame.size.height - 200;            // Remove area covered by keyboard
    
    CGFloat y = viewCenterY - availableHeight / 2.0;
    if (y < 0) {
        y = 0;
    }
    _isScrolled = YES;
    
    [self.scrollView setContentOffset:CGPointMake(0, y) animated:YES];
    
}

- (void) scrollBack
{
    if ( ! _isScrolled ) return;
    
    [self.scrollView setContentOffset:CGPointMake(0, 0) animated:YES];
    _isScrolled = NO;
}


- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    // The request is complete and data has been received
    // You can parse the stuff in your instance variable now
    
    NSError *jsonParsingError = nil;
    id object = [NSJSONSerialization JSONObjectWithData:_responseData options:0 error:&jsonParsingError];
    [_spinner stopAnimating];
    if (jsonParsingError) {
        NSLog(@"JSON ERROR: %@", [jsonParsingError localizedDescription]);
    } else {
        if([object isKindOfClass:[NSDictionary class]])
        {
            int responseCode = [[object objectForKey:@"response_code"] intValue];
            if(responseCode == 1) {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Reset Successful"
                                                                message:@"An email has been dispatched to your registered email account. Follow the instructions in the email to reset your password."
                                                               delegate:nil
                                                      cancelButtonTitle:@"OK"
                                                      otherButtonTitles:nil];
                [alert show];
                [self performSegueWithIdentifier:@"resetToLogin" sender:self];
            } else if(responseCode == 2) {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Reset error"
                                                                message:@"The username and email combination does not exist. If you forgot your username or email, please contact an administrator."
                                                               delegate:nil
                                                      cancelButtonTitle:@"OK"
                                                      otherButtonTitles:nil];
                [alert show];
            }else {
                //invalid server response
            }
        }
        else {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Server error"
                                                            message:@"A server error has occured, please try again"
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
        }
    }
}


- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    // The request has failed for some reason!
    // Check the error var
    [_spinner stopAnimating];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Login error"
                                                    message:[error localizedDescription]
                                                   delegate:nil
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];
}



@end
