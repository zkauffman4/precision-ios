//
//  MatterSearchViewController.m
//  Precision iOS
//
//  Created by Zach Kauffman on 6/1/14.
//  Copyright (c) 2014 Zach Kauffman. All rights reserved.
//

#import "TaskCodeViewController.h"
#import "TaskCodeObject.h"
#import "NewEntryViewController.h"
#import "AppDelegate.h"
#import "ViewController.h"

@interface TaskCodeViewController ()

@end

@implementation TaskCodeViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationItem.title = @"Tasks";
    NSString *url = [NSString stringWithFormat: @"https://precisiontimeentry.com/api/get_task_codes.php?code_group=%@&type=task", _code_group];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]];
    NSURLConnection *conn = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    _searchBar.delegate = (id)self;
}
-(void)setcodeGroup:(NSString *)code_group
{
    self.code_group = code_group;
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    if(_isFiltered) {
        return [_filteredTableData count];
    }
    return [_tasksArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    //[self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"MatterCell"];
    static NSString *CellIdentifier = @"MatterCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell==nil) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        
    }
    
    if(_isFiltered) {
        cell.detailTextLabel.text = ((TaskCodeObject*)[_filteredTableData objectAtIndex:indexPath.row]).codeId;
        cell.textLabel.text = ((TaskCodeObject*)[_filteredTableData objectAtIndex:indexPath.row]).codeName;
        cell.detailTextLabel.textColor = [UIColor grayColor];
        
    } else {
        cell.detailTextLabel.text = ((TaskCodeObject*)[_tasksArray objectAtIndex:indexPath.row]).codeId;
        cell.textLabel.text = ((TaskCodeObject*)[_tasksArray objectAtIndex:indexPath.row]).codeName;
        cell.detailTextLabel.textColor = [UIColor grayColor];
        
    }
    // Configure the cell...
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    TaskCodeObject *codeToSend;
    if(_isFiltered) {
        codeToSend = [_filteredTableData objectAtIndex:indexPath.row];
    } else {
        codeToSend = [_tasksArray objectAtIndex:indexPath.row];
    }
    [(NewEntryViewController*)self.delegate setTask:codeToSend];
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    _isFiltered = NO;
    [self.tableView reloadData];
}

#pragma mark NSURLConnection Delegate Methods

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    // A response has been received, this is where we initialize the instance var you created
    // so that we can append data to it in the didReceiveData method
    // Furthermore, this method is called each time there is a redirect so reinitializing it
    // also serves to clear it
    _responseData = [[NSMutableData alloc] init];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    // Append the new data to the instance variable you declared
    [_responseData appendData:data];
}

- (NSCachedURLResponse *)connection:(NSURLConnection *)connection
                  willCacheResponse:(NSCachedURLResponse*)cachedResponse {
    // Return nil to indicate not necessary to store a cached response for this connection
    return nil;
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    // The request is complete and data has been received
    // You can parse the stuff in your instance variable now
    
    NSError *jsonParsingError = nil;
    id object = [NSJSONSerialization JSONObjectWithData:_responseData options:0 error:&jsonParsingError];
    
    if (jsonParsingError) {
        NSLog(@"JSON ERROR: %@", [jsonParsingError localizedDescription]);
    } else {
        if([object isKindOfClass:[NSDictionary class]]) {
            //trying to delete?
            NSDictionary *results = object;
            //NSLog(@"Data: %@", [results description]);
            if([results objectForKey:@"tasks"]) {
                NSArray *tasks = [results objectForKey:@"tasks"];
                _tasksArray = [NSMutableArray array];
                for(NSDictionary *item in tasks) {
                    NSString *codeId = [item objectForKey:@"code_id"];
                    NSString *codeName = [item objectForKey:@"code_desc"];
                    
                    TaskCodeObject *task = [[TaskCodeObject alloc] initWithId:codeId andName:codeName];
                    
                    [_tasksArray addObject:task];
                }
                [self.tableView reloadData];
            } else if([results objectForKey:@"session"] != nil) {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Session Timeout"
                                                                message:@"You have had more than 15 minutes of inactivity. To protect your account, you have been logged out. Please log in again."
                                                               delegate:nil
                                                      cancelButtonTitle:@"OK"
                                                      otherButtonTitles:nil];
                [alert show];
                AppDelegate *appDelegate;
                appDelegate=(AppDelegate *)[[UIApplication sharedApplication] delegate];//in didLoad method
                UIStoryboard *storyboard = [UIStoryboard storyboardWithName:appDelegate.storyboardName bundle:nil];
                ViewController *v = [storyboard instantiateViewControllerWithIdentifier:@"loginController"];
                [self presentViewController:v animated:YES completion:nil];
            }

            else {
                [self.tableView reloadData];
            }
        } else {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Database error"
                                                            message:@"A database error has occurred"
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
        }
    }
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    // The request has failed for some reason!
    // Check the error var
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Login error"
                                                    message:[error localizedDescription]
                                                   delegate:nil
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];
}

-(void)searchBar:(UISearchBar*)searchBar textDidChange:(NSString*)text
{
    if(text.length == 0)
    {
        _isFiltered = FALSE;
    }
    else
    {
        _isFiltered = true;
        _filteredTableData = [[NSMutableArray alloc] init];
        
        for (TaskCodeObject* task in _tasksArray)
        {
            NSRange idRange = [task.codeId rangeOfString:text options:NSCaseInsensitiveSearch];
            NSRange nameRange = [task.codeName rangeOfString:text options:NSCaseInsensitiveSearch];
            if(nameRange.location != NSNotFound || idRange.location != NSNotFound)
            {
                [_filteredTableData addObject:task];
            }
        }
    }
    
    [self.tableView reloadData];
}

-(void)showActivityViewer
{
    AppDelegate *del = [[UIApplication sharedApplication] delegate];
    UIWindow *window = del.window;
    _activityView = [[UIView alloc] initWithFrame: CGRectMake(0, 0, window.bounds.size.width, window.bounds.size.height)];
    _activityView.backgroundColor = [UIColor blackColor];
    _activityView.alpha = 0.5;
    
    UIActivityIndicatorView *activityWheel = [[UIActivityIndicatorView alloc] initWithFrame: CGRectMake(window.bounds.size.width / 2 - 12, window.bounds.size.height / 2 - 12, 24, 24)];
    activityWheel.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhite;
    activityWheel.autoresizingMask = (UIViewAutoresizingFlexibleLeftMargin |
                                      UIViewAutoresizingFlexibleRightMargin |
                                      UIViewAutoresizingFlexibleTopMargin |
                                      UIViewAutoresizingFlexibleBottomMargin);
    [_activityView addSubview:activityWheel];
    [window addSubview: _activityView];
    
    [[[_activityView subviews] objectAtIndex:0] startAnimating];
}

-(void)hideActivityViewer
{
    [[[_activityView subviews] objectAtIndex:0] stopAnimating];
    [_activityView removeFromSuperview];
    _activityView = nil;
}

- (IBAction)reloadDataAction:(id)sender {
    [NSThread detachNewThreadSelector:@selector(showActivityViewer) toTarget:self withObject:nil];
    [self.tableView reloadData];
    [self hideActivityViewer];
}


/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
 {
 }
 */

/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */

/*
 #pragma mark - Navigation
 
 // In a story board-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
 {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 
 */

@end
