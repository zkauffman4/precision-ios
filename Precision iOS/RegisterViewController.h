//
//  RegisterViewController.h
//  Precision iOS
//
//  Created by Zach Kauffman on 6/11/14.
//  Copyright (c) 2014 Zach Kauffman. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RegisterViewController : UIViewController<UITextFieldDelegate, UIScrollViewDelegate>
@property (strong, nonatomic) IBOutlet UITextField *usernameTextfield;
@property (strong, nonatomic) IBOutlet UITextField *emailTextfield;
@property (strong, nonatomic) IBOutlet UITextField *confirmEmailTextfield;
@property (strong, nonatomic) IBOutlet UITextField *passwordTextfield;
@property (strong, nonatomic) IBOutlet UITextField *confirmPasswordTextfield;
@property (strong, nonatomic) IBOutlet UITextField *registrationCodeTextfield;
@property (strong, nonatomic) IBOutlet UITextField *billingIdTextfield;
@property (strong, nonatomic) UIActivityIndicatorView *spinner;
@property (strong, nonatomic) NSMutableData *responseData;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property CGFloat scrollDistance;
@property BOOL isScrolled;
-(IBAction)createBtnPressed:(id)sender;
-(IBAction)backBtnPressed:(id)sender;
@end
