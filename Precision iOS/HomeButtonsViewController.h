//
//  HomeButtonsViewController.h
//  Precision iOS
//
//  Created by Zach Kauffman on 5/29/14.
//  Copyright (c) 2014 Zach Kauffman. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HomeButtonsViewController : UIViewController
-(IBAction)pendingBtnPressed:(id)sender;
-(IBAction)historyBtnPressed:(id)sender;
-(IBAction)statsBtnPressed:(id)sender;
-(IBAction)newEntryBtnPressed:(id)sender;
-(IBAction)settingsBtnPressed:(id)sender;
-(IBAction)emailLogsBtnPressed:(id)sender;
@property (strong, nonatomic) NSMutableData *responseData;
@property (strong, nonatomic) NSMutableArray *inbox;
@property (strong, nonatomic) NSMutableArray *sent;
@property (strong, nonatomic) UIActivityIndicatorView *spinner;


@end
