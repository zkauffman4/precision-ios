//
//  HistorySevenDaysViewController.h
//  Precision iOS
//
//  Created by Zach Kauffman on 6/3/14.
//  Copyright (c) 2014 Zach Kauffman. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWTableViewCell.h"
#import "CustomTableViewCell.h"

@interface HistorySevenDaysViewController : UITableViewController<SWTableViewCellDelegate>
@property (nonatomic, strong) NSMutableData *responseData;
@property (nonatomic, strong) NSMutableArray *entryArray;
@end
