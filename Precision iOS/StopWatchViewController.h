//
//  StopWatchViewController.h
//  Precision iOS
//
//  Created by Zach Kauffman on 7/21/14.
//  Copyright (c) 2014 Zach Kauffman. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NewEntryViewController.h"

@interface StopWatchViewController : UIViewController
@property BOOL running, alreadyStarted;
@property (strong, nonatomic) IBOutlet UIImageView *startPauseImage;
@property NSTimeInterval startTime, totalElapsed;
@property (strong, nonatomic) IBOutlet UIButton *startBtn;
@property (strong, nonatomic) IBOutlet UILabel *timerLabel;
@property (strong, nonatomic) id delegate;
-(IBAction)startPausePressed:(id)sender;
-(IBAction)clearPressed:(id)sender;
-(IBAction)donePressed:(id)sender;
@end
