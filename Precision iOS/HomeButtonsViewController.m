//
//  HomeButtonsViewController.m
//  Precision iOS
//
//  Created by Zach Kauffman on 5/29/14.
//  Copyright (c) 2014 Zach Kauffman. All rights reserved.
//

#import "HomeButtonsViewController.h"
#import "EmailsCallsViewController.h"
#import "FBEncryptorAES.h"
#import "AppDelegate.h"
#import "ViewController.h"


@interface HomeButtonsViewController ()

@end

@implementation HomeButtonsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSString *lastLogin = [[NSUserDefaults standardUserDefaults] objectForKey:@"last_login"];
    
    NSString *e = [[NSUserDefaults standardUserDefaults] objectForKey:@"e"];
    
    NSString *username = [[NSUserDefaults standardUserDefaults] objectForKey:@"u_n"];
    NSRange isRange = [username rangeOfString:@"demouser" options:NSCaseInsensitiveSearch];
    if(isRange.location == 0) {
        //found the string
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Welcome to Precision!"
                                                        message:@"Thank you for trying Precision Time Entry. To learn more about Precision Time Entry and our team, please visit www.precisiontimeentry.com or feel free to email us at support@precisiontimeentry.com!"
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
    }

    if(e != nil && [[NSUserDefaults standardUserDefaults] boolForKey:@"emailLogs"] && ![e isEqualToString:@""]) {
        NSString* decrypted = [FBEncryptorAES decryptBase64String:e
                                                        keyString:@"@j7tkqb26"];
        
        NSString *dateBase64 = [[lastLogin dataUsingEncoding:NSUTF8StringEncoding] base64Encoding];
        
        NSString *url = [NSString stringWithFormat: @"https://precisiontimeentry.com/api/get_emails.php?password=%@&last_login=%@",decrypted, dateBase64];
        
        NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:url]];
        
        // Create url connection and fire request
        NSURLConnection *conn = [[NSURLConnection alloc] initWithRequest:request delegate:self];
        _spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        _spinner.opaque = NO;
        _spinner.backgroundColor = [UIColor colorWithWhite:0.0f alpha:0.6f];
        [_spinner setBounds:self.view.bounds];
        _spinner.center = self.view.center;
        _spinner.activityIndicatorViewStyle = UIActivityIndicatorViewStyleGray;
        [_spinner setColor:[UIColor colorWithRed:0.6 green:0.8 blue:1.0 alpha:1.0]];
        [self.view addSubview:_spinner]; // spinner is not visible until started
        [_spinner startAnimating];


    }
    //[[self navigationController] setNavigationBarHidden:YES animated:NO];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.topItem.title = @"Precision Home";
    //UIBarButtonItem *anotherButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAction target:self action:nil];
    //self.navigationItem.rightBarButtonItem = anotherButton;
	// Do any additional setup after loading the view.
    UIButton *backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage *backBtnImage = [UIImage imageNamed:@"logout.png"]  ;
    [backBtn setBackgroundImage:backBtnImage forState:UIControlStateNormal];
    [backBtn addTarget:self action:@selector(logout) forControlEvents:UIControlEventTouchUpInside];
    backBtn.frame = CGRectMake(0, 0, 36, 36);
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithCustomView:backBtn] ;
    self.navigationItem.leftBarButtonItem = backButton;
    
}

-(void)logout
{
    NSString *url = [NSString stringWithFormat: @"https://precisiontimeentry.com/api/logout.php"];
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:url]];
    
    // Create url connection and fire request
    NSURLConnection *conn = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    //send logout request and redirect to home screen
    [self performSegueWithIdentifier:@"logout" sender:self];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)pendingBtnPressed:(id)sender
{
    [self performSegueWithIdentifier:@"homeToPending" sender:self];
}

-(IBAction)historyBtnPressed:(id)sender
{
    [self performSegueWithIdentifier:@"homeToHistory" sender:self];
}

-(IBAction)statsBtnPressed:(id)sender
{
    [self performSegueWithIdentifier:@"homeToStats" sender:self];
}

-(IBAction)newEntryBtnPressed:(id)sender
{
    [self performSegueWithIdentifier:@"homeToNewEntry" sender:self];
}

-(IBAction)settingsBtnPressed:(id)sender
{
    [self performSegueWithIdentifier:@"homeToSettings" sender:self];
}

-(IBAction)emailLogsBtnPressed:(id)sender
{
    [self performSegueWithIdentifier:@"homeToLogs" sender:self];
}

#pragma mark NSURLConnection Delegate Methods

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    // A response has been received, this is where we initialize the instance var you created
    // so that we can append data to it in the didReceiveData method
    // Furthermore, this method is called each time there is a redirect so reinitializing it
    // also serves to clear it
    _responseData = [[NSMutableData alloc] init];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    // Append the new data to the instance variable you declared
    [_responseData appendData:data];
}

- (NSCachedURLResponse *)connection:(NSURLConnection *)connection
                  willCacheResponse:(NSCachedURLResponse*)cachedResponse {
    // Return nil to indicate not necessary to store a cached response for this connection
    return nil;
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    // The request is complete and data has been received
    // You can parse the stuff in your instance variable now
    [_spinner stopAnimating];
    NSError *jsonParsingError = nil;
    id object = [NSJSONSerialization JSONObjectWithData:_responseData options:0 error:&jsonParsingError];
    
    if (jsonParsingError) {
        NSLog(@"JSON ERROR: %@", [jsonParsingError localizedDescription]);
    } else {
        if([object isKindOfClass:[NSDictionary class]])
        {
            NSDictionary *dic = [object objectForKey:@"mail"];
            NSNumber *err;
            @try {
                err = [object objectForKey:@"error_code"];
            }
            @catch (NSException *e) {
                
            }
            if(dic!= nil && dic != (id)[NSNull null]) {
                _inbox = [dic objectForKey:@"inbox"];
                _sent = [dic objectForKey:@"sent"];
                
                if([_inbox count] > 0 || [_sent count] > 0) {
                    
                    UIAlertView *pop = [[UIAlertView alloc] initWithTitle:@"New Emails"
                                                                  message:@"You have sent or received emails since last login, would you like to create entries?"
                                                                 delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
                    [pop show];
                }
            } else if(err != nil && err != (id)[NSNull null]) {
                if([err isEqualToNumber:[NSNumber numberWithInt:1]]) {
                    //email or username is blank
                } else if([err isEqualToNumber:[NSNumber numberWithInt:2]]) {
                    //couldn't connect, invalid credentials?
                    UIAlertView *pop = [[UIAlertView alloc] initWithTitle:@"Email connection error"
                                                                  message:@"Could not connect to mail server. Are you sure your login credentials are correct?"
                                                                 delegate:self cancelButtonTitle:@"Continue" otherButtonTitles:nil];
                    [pop show];
                }
            }            
        }else {
            NSString *d = [[NSString alloc] initWithData:_responseData encoding:NSUTF8StringEncoding];
        }
    }
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    // The request has failed for some reason!
    // Check the error var
    [_spinner stopAnimating];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Login error"
                                                    message:[error localizedDescription]
                                                   delegate:nil
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(buttonIndex == 1) {
        //they said yes?
        
        AppDelegate *appDelegate;
        appDelegate=(AppDelegate *)[[UIApplication sharedApplication] delegate];//in didLoad method
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:appDelegate.storyboardName bundle:nil];
        EmailsCallsViewController *v = [storyboard instantiateViewControllerWithIdentifier:@"emailsCalls"];
        v.inbox = [[NSMutableArray alloc] initWithArray:_inbox];
        v.sent = [[NSMutableArray alloc] initWithArray:_sent];
        [self.navigationController pushViewController:v animated:YES];
    }
}


@end
