//
//  GraphViewController.m
//  Precision iOS
//
//  Created by Zach Kauffman on 6/5/14.
//  Copyright (c) 2014 Zach Kauffman. All rights reserved.
//

#import "GraphViewController.h"
#import "PNChart.h"
#import "PNLineChartData.h"
#import "PNLineChartDataItem.h"
#import "AppDelegate.h"
#import "ViewController.h"

@interface GraphViewController ()

@end

@implementation GraphViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    BOOL hours = [_type isEqualToString:@"hours"];
    BOOL days = [_period isEqualToString:@"day"];
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;
    CGFloat screenHeight = screenRect.size.height;

    self.barChart = [[PNBarChart alloc] initWithFrame:CGRectMake(0, 0, screenWidth, screenHeight -100)];
    self.barChart.backgroundColor = [UIColor clearColor];
    self.barChart.yLabelFormatter = ^(CGFloat yValue){
        CGFloat yValueParsed = yValue;
        NSString * labelText = [NSString stringWithFormat:@"%1.f",yValueParsed];
        return labelText;
    };
    self.barChart.labelMarginTop = 5.0;
    
    [self.barChart setStrokeColor:PNFreshGreen];
    self.barChart.delegate = self;
    
    
    NSString *url = [NSString stringWithFormat: @"https://precisiontimeentry.com/api/get_graph_data.php?period=%@&type=%@", _period, _type];
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:url]];
    
    // Create url connection and fire request
    NSURLConnection *conn = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    
    _spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    _spinner.opaque = NO;
    _spinner.backgroundColor = [UIColor colorWithWhite:0.0f alpha:0.6f];
    [_spinner setBounds:self.view.bounds];
    _spinner.center = self.view.center;
    _spinner.activityIndicatorViewStyle = UIActivityIndicatorViewStyleGray;
    [_spinner setColor:[UIColor colorWithRed:0.6 green:0.8 blue:1.0 alpha:1.0]];
    [self.view addSubview:_spinner]; // spinner is not visible until started
    [_spinner startAnimating];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark NSURLConnection Delegate Methods

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    
    _responseData = [[NSMutableData alloc] init];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    [_responseData appendData:data];
}

- (NSCachedURLResponse *)connection:(NSURLConnection *)connection
                  willCacheResponse:(NSCachedURLResponse*)cachedResponse {
    
    return nil;
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    
    [_spinner stopAnimating];
    NSError *jsonParsingError = nil;
    id object = [NSJSONSerialization JSONObjectWithData:_responseData options:0 error:&jsonParsingError];
    
    if (jsonParsingError) {
        NSLog(@"JSON ERROR: %@", [jsonParsingError localizedDescription]);
    } else {
        if([object isKindOfClass:[NSDictionary class]])
        {
            if([object objectForKey:@"session"] != nil) {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Session Timeout"
                                                                message:@"You have had more than 15 minutes of inactivity. To protect your account, you have been logged out. Please log in again."
                                                               delegate:nil
                                                      cancelButtonTitle:@"OK"
                                                      otherButtonTitles:nil];
                [alert show];
                AppDelegate *appDelegate;
                appDelegate=(AppDelegate *)[[UIApplication sharedApplication] delegate];//in didLoad method
                UIStoryboard *storyboard = [UIStoryboard storyboardWithName:appDelegate.storyboardName bundle:nil];
                ViewController *v = [storyboard instantiateViewControllerWithIdentifier:@"loginController"];
                [self presentViewController:v animated:YES completion:nil];
            } else {

                BOOL hours = [_type isEqualToString:@"hours"];
                BOOL entries = [_type isEqualToString:@"entries"];
                BOOL matters = [_type isEqualToString:@"matters"];
                BOOL days = [_period isEqualToString:@"day"];
                BOOL weeks = [_period isEqualToString:@"week"];
                BOOL months = [_period isEqualToString:@"month"];
                CGRect screenRect = [[UIScreen mainScreen] bounds];
                CGFloat screenWidth = screenRect.size.width;
                CGFloat screenHeight = screenRect.size.height;
                UILabel *xAxisLabel = [[UILabel alloc] initWithFrame:CGRectMake(screenWidth/2, screenHeight*.8, 100, 40)];
                
                
                NSArray *keys, *values;
                if(hours && days) {
                    self.title = @"Hours by Day";
                    xAxisLabel.text = @"Day";
                    keys = [self getOrderWeekDictionaryKeys:object];
                    values = [self getOrderWeekDictionaryValues:object];
                } else if(hours && weeks) {
                    xAxisLabel.text = @"Week of";
                    self.title = @"Hours by Week";
                    NSArray *hoursArray = [object objectForKey:@"hours"];
                    keys = [self getOrderMonthDictionaryKeys:hoursArray];
                    values = [self getOrderMonthDictionaryValues:hoursArray];
                    
                } else if(hours && months) {
                    xAxisLabel.text = @"Month";
                    self.title = @"Hours by Month";
                    keys = [self getOrderYearDictionaryKeys:object];
                    values = [self getOrderYearDictionaryValues:object];
                } else if(matters && days) {
                    xAxisLabel.text = @"Day";
                    self.title = @"Matters by Day";
                    keys = [self getOrderWeekDictionaryKeys:object];
                    values = [self getOrderWeekDictionaryValues:object];
                } else if(matters && weeks) {
                    xAxisLabel.text = @"Week of";
                    self.title = @"Matters by Week";
                    @try {
                        NSArray *matters = [object objectForKey:@"hours"];
                        keys = [self getOrderMonthDictionaryKeys:matters];
                        values = [self getOrderMonthDictionaryValues:matters];
                        
                    } @catch (NSException *exception) {
                        NSLog(@"A database error has occured");
                    }
                } else if(matters && months) {
                    xAxisLabel.text = @"Month";
                    self.title = @"Matters by Month";
                    keys = [self getOrderYearDictionaryKeys:object];
                    values = [self getOrderYearDictionaryValues:object];
                } else if(entries && days) {
                    xAxisLabel.text = @"Day";
                    self.title = @"Entries by day";
                    keys = [self getOrderWeekDictionaryKeys:object];
                    values = [self getOrderWeekDictionaryValues:object];
                } else if(entries && weeks) {
                    xAxisLabel.text = @"Week of";
                    self.title = @"Entries by Week";
                    @try {
                        NSArray *entries = [object objectForKey:@"hours"];
                        keys = [self getOrderMonthDictionaryKeys:entries];
                        values = [self getOrderMonthDictionaryValues:entries];
                        
                    } @catch (NSException *exception) {
                        NSLog(@"A database error has occured");
                    }
                } else if(entries && months) {
                    xAxisLabel.text = @"Month";
                    self.title = @"Entries by Month";
                    keys = [self getOrderYearDictionaryKeys:object];
                    values = [self getOrderYearDictionaryValues:object];
                } else {
                    NSLog(@"An error has occured, please go back and try again");
                }
                [xAxisLabel sizeToFit];
                xAxisLabel.center = CGPointMake(screenWidth/2, screenHeight*.85);
                [self.view addSubview:xAxisLabel];
                [self.barChart setXLabels:keys];
                [self.barChart setYValues:values];
                /*
                int maxY = [self getYValueMax:values];
                int multiplier = floor(((float)maxY/(float)10) + .999);
                
                if(maxY % multiplier != 0) {
                    while(maxY % multiplier != 0)
                    {
                        maxY++;
                    }
                }
                self.barChart.yValueMax = maxY;
                
                self.barChart.yLabelSum = maxY/multiplier;
                 */
                
                float max = [[values valueForKeyPath:@"@max.floatValue"] floatValue];
                
                if(max < 50) {
                    int multiplier = (max/10)+1;
                    int increments = ((max/multiplier)+1);
                    self.barChart.yValueMax = (increments*multiplier);
                    self.barChart.yLabelSum = increments;
                    
                    
                } else if(max >= 50 && max < 500) {
                    int multiplier = (max/100)+1;
                    int mult = multiplier*10;
                    int increments = ((max/mult)+1);
                    self.barChart.yValueMax = (increments*mult);
                    self.barChart.yLabelSum = increments;
                    
                } else {
                    int multiplier = (max/1000)+1;
                    int mult = multiplier*100;
                    int increments = ((max/mult)+1);
                    self.barChart.yValueMax = (increments*mult);
                    self.barChart.yLabelSum = increments;
                }
                
                [self.barChart strokeChart];
                [self.view addSubview:self.barChart];
            }
        }
        else
        {
            NSString *d = [[NSString alloc] initWithData:_responseData encoding:NSUTF8StringEncoding];
        }
    }
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    
    [_spinner stopAnimating];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Login error"
                                                    message:[error localizedDescription]
                                                   delegate:nil
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];
}

-(NSMutableArray*)getOrderWeekDictionaryValues: (NSDictionary*)dic
{
    NSMutableArray *ret = [NSMutableArray new];
    [ret addObject:[dic objectForKey:@"Sun"]];
    [ret addObject:[dic objectForKey:@"Mon"]];
    [ret addObject:[dic objectForKey:@"Tues"]];
    [ret addObject:[dic objectForKey:@"Wed"]];
    [ret addObject:[dic objectForKey:@"Thurs"]];
    [ret addObject:[dic objectForKey:@"Fri"]];
    [ret addObject:[dic objectForKey:@"Sat"]];
    return [ret copy];
}

-(NSMutableArray*)getOrderWeekDictionaryKeys: (NSDictionary*)dic
{
    NSMutableArray *ret = [NSMutableArray new];
    [ret addObject:@"Sun"];
    [ret addObject:@"Mon"];
    [ret addObject:@"Tues"];
    [ret addObject:@"Wed"];
    [ret addObject:@"Thurs"];
    [ret addObject:@"Fri"];
    [ret addObject:@"Sat"];
    return [ret copy];
}

-(NSMutableArray*)getOrderYearDictionaryValues: (NSDictionary*)dic
{
    NSMutableArray *ret = [NSMutableArray new];
    [ret addObject:[dic objectForKey:@"Jan"]];
    [ret addObject:[dic objectForKey:@"Feb"]];
    [ret addObject:[dic objectForKey:@"Mar"]];
    [ret addObject:[dic objectForKey:@"Apr"]];
    [ret addObject:[dic objectForKey:@"May"]];
    [ret addObject:[dic objectForKey:@"Jun"]];
    [ret addObject:[dic objectForKey:@"Jul"]];
    [ret addObject:[dic objectForKey:@"Aug"]];
    [ret addObject:[dic objectForKey:@"Sept"]];
    [ret addObject:[dic objectForKey:@"Oct"]];
    [ret addObject:[dic objectForKey:@"Nov"]];
    [ret addObject:[dic objectForKey:@"Dec"]];
    return [ret copy];
}

-(NSMutableArray*)getOrderYearDictionaryKeys: (NSDictionary*)dic
{
    NSMutableArray *ret = [NSMutableArray new];
    [ret addObject:@"Jan"];
    [ret addObject:@"Feb"];
    [ret addObject:@"Mar"];
    [ret addObject:@"Apr"];
    [ret addObject:@"May"];
    [ret addObject:@"Jun"];
    [ret addObject:@"Jul"];
    [ret addObject:@"Aug"];
    [ret addObject:@"Sept"];
    [ret addObject:@"Oct"];
    [ret addObject:@"Nov"];
    [ret addObject:@"Dec"];
    return [ret copy];
}

-(NSMutableArray*)getOrderMonthDictionaryValues: (NSArray*)array
{
    NSMutableArray *ret = [[NSMutableArray alloc] initWithCapacity:[array count]];
    for(int i = 0; i < [array count]; i++) {
        NSDictionary *dictionary = [array objectAtIndex:i];
        int index = [[dictionary objectForKey:@"index"] intValue];
        NSString *total = [dictionary objectForKey:@"count"];
        
        [ret insertObject:total atIndex:index];
    }
    
    return [ret copy];
}

-(NSMutableArray*)getOrderMonthDictionaryKeys: (NSArray*)array
{
    NSMutableArray *ret = [NSMutableArray new];
    for(int i = 0; i < [array count]; i++) {
        NSDictionary *dictionary = [array objectAtIndex:i];
        int index = [[dictionary objectForKey:@"index"] intValue];
        NSString *prev = [dictionary objectForKey:@"prev_sunday"];
        //NSString *curr = [dictionary objectForKey:@"curr_sunday"];
        prev = [prev substringFromIndex:5];
        //curr = [curr substringFromIndex:5];
        NSString *key = [NSString stringWithFormat:@"%@", prev];
        [ret insertObject:key atIndex:index];
    }
    return [ret copy];
}

- (float)getYValueMax:(NSArray *)yLabels
{
    float max = [[yLabels valueForKeyPath:@"@max.floatValue"] floatValue];
    
    float ret = (int)roundf(max+.5);
    
    if (ret == 0) {
        return 1;
    }
    return ret;
}


@end
