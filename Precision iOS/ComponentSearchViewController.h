//
//  ComponentSearchViewController.h
//  Precision iOS
//
//  Created by Zach Kauffman on 6/2/14.
//  Copyright (c) 2014 Zach Kauffman. All rights reserved.
//
#import <UIKit/UIKit.h>

@interface ComponentSearchViewController : UITableViewController<UISearchBarDelegate, UISearchDisplayDelegate, UITableViewDataSource, UITableViewDelegate>
@property (strong, nonatomic) IBOutlet UISearchBar *searchBar;
@property (strong, nonatomic) NSMutableArray* componentsArray;
@property (strong, nonatomic) NSMutableArray* filteredTableData;
@property (strong, nonatomic) NSMutableData *responseData;
@property (nonatomic, weak) id delegate;
@property BOOL isFiltered;
@property (strong, nonatomic) NSString *code_group;
-(void)setcodeGroup:(NSString *)code_group;
@end
