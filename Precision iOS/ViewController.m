//
//  ViewController.m
//  Precision iOS
//
//  Created by Zach Kauffman on 5/29/14.
//  Copyright (c) 2014 Zach Kauffman. All rights reserved.
//

#import "ViewController.h"
#import "RSA.h"

@interface ViewController ()

@end

@implementation ViewController {
    CGFloat _yPositionStore;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    _txt_password.secureTextEntry = YES;
	_txt_password.delegate = self;
    _txt_username.delegate = self;
    _txt_password.borderStyle = UITextBorderStyleRoundedRect;
    _txt_username.borderStyle = UITextBorderStyleRoundedRect;
    NSString *u = [[NSUserDefaults standardUserDefaults] objectForKey:@"u_n"];
    _txt_username.text = u;
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)resetBtnPressed:(id)sender
{
    [self performSegueWithIdentifier:@"loginToReset" sender:self];
}

-(IBAction)createBtnPressed:(id)sender
{
    [self performSegueWithIdentifier:@"loginToRegistration" sender:self];
}

- (IBAction)loginBtn:(id)sender
{    
    NSString *test_username = _txt_username.text;
    NSString *test_password = _txt_password.text;
    //NSString *key = @"a16byteslongkey!a16byteslongkey!";
    
    //NSString *ciphertext = [test_password AES256EncryptWithKey: key];
    
    //NSString *salttest_password = [NSString stringWithFormat:@"%@%@", salt, test_password];
    
    //test_password = [salttest_password MD5];
    //RSA *rsa = [[RSA alloc] init];
    //test_username = [rsa encryptToString:test_username];
    //test_password = [rsa encryptToString:test_password];
    
    NSString *url = [NSString stringWithFormat: @"https://precisiontimeentry.com/api/login.php"];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]];
    [request setHTTPMethod:@"POST"];
    NSString *postString = [NSString stringWithFormat:@"u=%@&p=%@", test_username, test_password];
    [request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    
    // Create url connection and fire request
    //NSURLConnection *conn = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    
    
    
    
    if((![test_username isEqualToString: @""]) && (![_txt_password.text isEqualToString: @""]))
    {
        _spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        _spinner.opaque = NO;
        _spinner.backgroundColor = [UIColor colorWithWhite:0.0f alpha:0.6f];
        [_spinner setBounds:self.view.bounds];
        _spinner.center = self.view.center;
        _spinner.activityIndicatorViewStyle = UIActivityIndicatorViewStyleGray;
        [_spinner setColor:[UIColor colorWithRed:0.6 green:0.8 blue:1.0 alpha:1.0]];
        [self.view addSubview:_spinner]; // spinner is not visible until started
        [_spinner startAnimating];
        // Create url connection and fire request
        NSURLConnection *conn = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Login error"
                                                        message:@"You must enter username and password!"
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        _txt_username.layer.borderColor=[[UIColor blackColor]CGColor];
        _txt_password.layer.borderColor=[[UIColor blackColor]CGColor];
        if([test_username isEqualToString: @""]) {
            _txt_username.layer.borderColor=[[UIColor redColor]CGColor];
            _txt_username.layer.borderWidth= 1.0f;
        }
        if([test_password isEqualToString: @""]) {
            _txt_password.layer.borderColor=[[UIColor redColor]CGColor];
            _txt_password.layer.borderWidth= 1.0f;
        }
        
    }
}

#pragma mark NSURLConnection Delegate Methods

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    // A response has been received, this is where we initialize the instance var you created
    // so that we can append data to it in the didReceiveData method
    // Furthermore, this method is called each time there is a redirect so reinitializing it
    // also serves to clear it
    _responseData = [[NSMutableData alloc] init];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    // Append the new data to the instance variable you declared
    [_responseData appendData:data];
}

- (NSCachedURLResponse *)connection:(NSURLConnection *)connection
                  willCacheResponse:(NSCachedURLResponse*)cachedResponse {
    // Return nil to indicate not necessary to store a cached response for this connection
    return nil;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    [self scrollBack];
    return YES;
}

- (BOOL)textFieldShouldBeginEditing:(UITextView *)textField{
    
    [self scrollViewToCenterOfScreen:textField];
    return YES;
    
}

- (void)scrollViewToCenterOfScreen:(UIView *)theView {
    CGFloat viewCenterY = theView.center.y;
    CGRect applicationFrame = [[UIScreen mainScreen] applicationFrame];
    
    CGFloat availableHeight = applicationFrame.size.height - 200;            // Remove area covered by keyboard
    
    CGFloat y = viewCenterY - availableHeight / 2.0;
    if (y < 0) {
        y = 0;
    }
    _isScrolled = YES;
    
    [self.scrollView setContentOffset:CGPointMake(0, y) animated:YES];
    
}

- (void) scrollBack
{
    if ( ! _isScrolled ) return;
    
    [self.scrollView setContentOffset:CGPointMake(0, 0) animated:YES];
    _isScrolled = NO;
}


-(IBAction)demoBtnPressed:(id)sender
{
    NSString *url = [NSString stringWithFormat: @"https://precisiontimeentry.com/api/login.php"];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]];
    [request setHTTPMethod:@"POST"];
    NSString *postString = [NSString stringWithFormat:@"u=%@&p=%@", @"demo", @"password"];
    [request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    NSURLConnection *conn = [[NSURLConnection alloc] initWithRequest:request delegate:self];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    // The request is complete and data has been received
    // You can parse the stuff in your instance variable now
    
    NSError *jsonParsingError = nil;
    id object = [NSJSONSerialization JSONObjectWithData:_responseData options:0 error:&jsonParsingError];
    [_spinner stopAnimating];
    if (jsonParsingError)
    {
        NSLog(@"JSON ERROR: %@", [jsonParsingError localizedDescription]);
    }
    else
    {
        if([object isKindOfClass:[NSDictionary class]])
        {
            NSDictionary *results = object;
            if([results objectForKey:@"error"])
            {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Login error!"
                                                                message:@"Invalid login credentials. Please try again."
                                                               delegate:nil
                                                      cancelButtonTitle:@"OK"
                                                      otherButtonTitles:nil];
                [alert show];
            }
            else if([results objectForKey:@"demofail"])
            {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Login error!"
                                                                message:@"Too many demo users currently logged in. Please try again in a few moments."
                                                               delegate:nil
                                                      cancelButtonTitle:@"OK"
                                                      otherButtonTitles:nil];
                [alert show];
            }
            else
            {
                NSString *email = [results objectForKey:@"exchange_email"];
                if(email == nil || email == (id)[NSNull null] || [email isEqualToString:@""]) {
                    email = [results objectForKey:@"email"];
                }
                NSString *lastLogin = [results objectForKey:@"last_login"];
                if(lastLogin == nil || lastLogin == (id)[NSNull null]) {
                    lastLogin = @"";
                }
                [[NSUserDefaults standardUserDefaults] setObject:lastLogin forKey:@"last_login"];
                [[NSUserDefaults standardUserDefaults] setObject:email forKey:@"exchange_email"];
                if(![[[NSUserDefaults standardUserDefaults] objectForKey:@"u_n"] isEqualToString:[results objectForKey:@"username"]]) {
                    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"e"];
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Settings Cleared!"
                                                                    message:@"You were not the last person to log in on this device. For security purposes, your exchange settings were cleared. Please re-enter your information in the settings tab."
                                                                   delegate:nil
                                                          cancelButtonTitle:@"OK"
                                                          otherButtonTitles:nil];
                    [alert show];
                }
                [[NSUserDefaults standardUserDefaults] setObject:[results objectForKey:@"username"] forKey:@"u_n"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                [self performSegueWithIdentifier:@"loginToHome" sender:self];
            }
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Login error!"
                                                            message:@"A server error has occured"
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
        }
    }
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    // The request has failed for some reason!
    // Check the error var
    [_spinner stopAnimating];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Login error"
                                                    message:[error localizedDescription]
                                                   delegate:nil
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];
}


- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    NSLog(@"touchesBegan:withEvent:");
    [self.view endEditing:YES];
    [super touchesBegan:touches withEvent:event];
}

@end
