//
//  EmailsCallsViewController.m
//  Precision iOS
//
//  Created by Zach Kauffman on 6/6/14.
//  Copyright (c) 2014 Zach Kauffman. All rights reserved.
//

#import "EmailsCallsViewController.h"
#import "NSData+Base64.h"
#import "LogsCell.h"
#import "AppDelegate.h"
#import "ViewController.h"

@interface EmailsCallsViewController ()

@end

@implementation EmailsCallsViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    CGRect frame = self.submitEntriesBar.frame;
    frame.origin.y = self.tableView.contentOffset.y + self.tableView.frame.size.height - self.submitEntriesBar.frame.size.height;
    self.submitEntriesBar.frame = frame;
    
    [self.view bringSubviewToFront:self.submitEntriesBar];
}

-(void)viewDidAppear:(BOOL)animated
{
    [self scrollViewDidScroll:self.tableView];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    _selectedPaths = [NSMutableArray new];
    _selectedItems = [NSMutableArray new];
    self.tableView.allowsMultipleSelection = YES;
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    UIBarButtonItem *s = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    
    UIBarButtonItem *d = [[UIBarButtonItem alloc] initWithTitle:@"Submit Selected Entries" style:UIBarButtonItemStyleDone target:self action:@selector(submitEntries)];
    _submitEntriesBar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, screenRect.size.height, screenRect.size.width, 50)];
    [_submitEntriesBar setItems:[[NSArray alloc] initWithObjects: s, d, nil]];
    [self.view addSubview:_submitEntriesBar];
    
    
    _submitEntriesBar.hidden = YES;

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    for(int i = 0; i < [self.inbox count]; i++) {
        NSMutableDictionary *arr =  [[NSMutableDictionary alloc] initWithDictionary:[_inbox objectAtIndex:i]];
        NSString *oldDate = [arr objectForKey:@"DateTime"];
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"MMM-dd-yyyy HH:mm:ss z"];
        [dateFormatter setTimeZone:[NSTimeZone defaultTimeZone]];
        NSDate *newDate = [dateFormatter dateFromString:oldDate];
        NSDateFormatter *dateFormatter2 = [[NSDateFormatter alloc] init];
        [dateFormatter2 setDateFormat:@"yyyy-MM-dd HH:mm"];
        NSString *newDateStr = [dateFormatter2 stringFromDate:newDate];
        [arr setObject:newDateStr forKey:@"DateTime"];
        [_inbox setObject:arr atIndexedSubscript:i];
    }
    
    for(int i = 0; i < [self.sent count]; i++) {
        NSMutableDictionary *arr =  [[NSMutableDictionary alloc] initWithDictionary:[_sent objectAtIndex:i]];
        NSString *oldDate = [arr objectForKey:@"DateTime"];
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"MMM-dd-yyyy HH:mm:ss z"];
        [dateFormatter setTimeZone:[NSTimeZone defaultTimeZone]];
        NSDate *newDate = [dateFormatter dateFromString:oldDate];
        NSDateFormatter *dateFormatter2 = [[NSDateFormatter alloc] init];
        [dateFormatter2 setDateFormat:@"yyyy-MM-dd HH:mm"];
        NSString *newDateStr = [dateFormatter2 stringFromDate:newDate];
        [arr setObject:newDateStr forKey:@"DateTime"];
        [_sent setObject:arr atIndexedSubscript:i];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)submitEntries
{
    //button clicked to submit entries
    NSError *error;
    NSString *str = @"";
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:_selectedItems
                                                       options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                         error:&error];
    
    if (! jsonData) {
        NSLog(@"Got an error: %@", error);
    } else {
        str = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }

    
    NSData *plainTextData = [str dataUsingEncoding:NSUTF8StringEncoding];
    NSString *base64String = [plainTextData base64EncodedString];
    
    NSString *url = [NSString stringWithFormat: @"https://precisiontimeentry.com/api/create_log_entries.php"];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]];
    [request setHTTPMethod:@"POST"];
    NSString *postString = [NSString stringWithFormat:@"json=%@", base64String];
    [request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    
    // Create url connection and fire request
    NSURLConnection *conn = [[NSURLConnection alloc] initWithRequest:request delegate:self];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    if (section==0)
    {
        return [_inbox count];
    }
    else{
        return [_sent count];
    }
}


- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    // create the parent view that will hold header Label
    CGRect window = [[UIScreen mainScreen] bounds];
    UIView* customView = [[UIView alloc] initWithFrame:CGRectMake(0, 0.0, window.size.width, 50)];
    customView.backgroundColor = [UIColor blackColor];
    // create the button object
    UILabel * headerLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    headerLabel.opaque = NO;
    headerLabel.textColor = [UIColor whiteColor];
    headerLabel.highlightedTextColor = [UIColor whiteColor];
    headerLabel.font = [UIFont boldSystemFontOfSize:20];
    headerLabel.frame = CGRectMake(20.0, 0.0, 300.0, 20);
    
    // If you want to align the header text as centered
    // headerLabel.frame = CGRectMake(150.0, 0.0, 300.0, 44.0);
    if(section == 0)
        headerLabel.text = @"Inbox"; // i.e. array element
    else
        headerLabel.text = @"Sent Items";
    [customView addSubview:headerLabel];
    
    return customView;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    LogsCell *cell = (LogsCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    if([self.selectedPaths containsObject:indexPath]){
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }else{
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    
    if (indexPath.section==0) {
        NSDictionary *dic = [_inbox objectAtIndex:indexPath.row];
        NSString *cellValue = [dic objectForKey:@"subject"];
        NSString *detail = [dic objectForKey:@"email"];
        NSString *date = [dic objectForKey:@"DateTime"];
        if(detail != nil &&detail != (id)[NSNull null] && [detail length] > 30) {
            cell.emailLabel.text = [detail substringToIndex:30];
        } else {
            cell.emailLabel.text = detail;
        }
        cell.subjectLabel.text = cellValue;
        cell.dateLabel.text = date;
    }
    else {
        NSDictionary *dic = [_sent objectAtIndex:indexPath.row];
        NSString *cellValue = [dic objectForKey:@"subject"];
        NSString *to = [dic objectForKey:@"displayName"];
        NSString *date = [dic objectForKey:@"DateTime"];
        if(to != nil && to != (id)[NSNull null] && [to length] > 30) {
            cell.emailLabel.text = [to substringToIndex:30];
        } else {
            cell.emailLabel.text = to;
        }
        cell.subjectLabel.text = cellValue;
        cell.dateLabel.text = date;
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)path {
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:path];
    //cell.selectionStyle = UITableViewCellSelectionStyleNone;
    if (cell.accessoryType == UITableViewCellAccessoryCheckmark) {
        cell.accessoryType = UITableViewCellAccessoryNone;
        if(path.section == 0) {
            [_selectedItems removeObject:[_inbox objectAtIndex:path.row]];
        } else if(path.section == 1) {
            [_selectedItems removeObject:[_sent objectAtIndex:path.row]];
        }
        [_selectedPaths removeObject:path];
        int count = (int)[_selectedItems count];
        
        if(count == 0) {
            _submitEntriesBar.hidden = YES;
        } else {
            _submitEntriesBar.hidden = NO;
        }
    } else {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
        _submitEntriesBar.hidden = NO;
        if(path.section == 0) {
            [_selectedItems addObject:[_inbox objectAtIndex:path.row]];
        } else if(path.section == 1) {
            [_selectedItems addObject:[_sent objectAtIndex:path.row]];
        }
        [_selectedPaths addObject:path];
    }
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)path {
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:path];
    //cell.selectionStyle = UITableViewCellSelectionStyleNone;
    if (cell.accessoryType == UITableViewCellAccessoryCheckmark) {
        cell.accessoryType = UITableViewCellAccessoryNone;
        if(path.section == 0) {
            [_selectedItems removeObject:[_inbox objectAtIndex:path.row]];
        } else if(path.section == 1) {
            [_selectedItems removeObject:[_sent objectAtIndex:path.row]];
        }
        [_selectedPaths removeObject:path];
        int count = (int)[_selectedItems count];
        
        if(count == 0) {
            _submitEntriesBar.hidden = YES;
        } else {
            _submitEntriesBar.hidden = NO;
        }
    } else {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
        if(path.section == 0) {
            [_selectedItems addObject:[_inbox objectAtIndex:path.row]];
        } else if(path.section == 1) {
            [_selectedItems addObject:[_sent objectAtIndex:path.row]];
        }
        [_selectedPaths addObject:path];
        _submitEntriesBar.hidden = NO;
    }
}

#pragma mark NSURLConnection Delegate Methods

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    // A response has been received, this is where we initialize the instance var you created
    // so that we can append data to it in the didReceiveData method
    // Furthermore, this method is called each time there is a redirect so reinitializing it
    // also serves to clear it
    _responseData = [[NSMutableData alloc] init];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    // Append the new data to the instance variable you declared
    [_responseData appendData:data];
}

- (NSCachedURLResponse *)connection:(NSURLConnection *)connection
                  willCacheResponse:(NSCachedURLResponse*)cachedResponse {
    // Return nil to indicate not necessary to store a cached response for this connection
    return nil;
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    // The request is complete and data has been received
    // You can parse the stuff in your instance variable now
    
    NSError *jsonParsingError = nil;
    id object = [NSJSONSerialization JSONObjectWithData:_responseData options:0 error:&jsonParsingError];
    NSMutableArray *paths = [NSMutableArray new];
    if (jsonParsingError) {
        NSLog(@"JSON ERROR: %@", [jsonParsingError localizedDescription]);
    }
    else
    {
        if([object isKindOfClass:[NSDictionary class]])
        {
            NSDictionary *err = [object objectForKey:@"error"];
            if(err != nil)
            {
                //error
            } else if([object objectForKey:@"session"] != nil) {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Session Timeout"
                                                                message:@"You have had more than 15 minutes of inactivity. To protect your account, you have been logged out. Please log in again."
                                                               delegate:nil
                                                      cancelButtonTitle:@"OK"
                                                      otherButtonTitles:nil];
                [alert show];
                AppDelegate *appDelegate;
                appDelegate=(AppDelegate *)[[UIApplication sharedApplication] delegate];//in didLoad method
                UIStoryboard *storyboard = [UIStoryboard storyboardWithName:appDelegate.storyboardName bundle:nil];
                ViewController *v = [storyboard instantiateViewControllerWithIdentifier:@"loginController"];
                [self presentViewController:v animated:YES completion:nil];
            }

            else
            {
                //success, remove rows from table
                for(int i = 0; i < self.tableView.numberOfSections; i++)
                {
                    for(int j = (int)[self.tableView numberOfRowsInSection:i]-1; j >= 0; j--)
                    {
                        NSIndexPath *path = [NSIndexPath indexPathForRow:j inSection:i];
                        UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:path];
                        if(cell.accessoryType == UITableViewCellAccessoryCheckmark) {
                            [paths addObject:path];
                            if(i == 0) {
                                [_inbox removeObjectAtIndex:path.row];
                            } else if(i == 1) {
                                [_sent removeObjectAtIndex:path.row];
                            }
                            [_selectedPaths removeObject:path];
                        }
                    }
                }
                //[_selectedItems removeObjects
                [self.tableView deleteRowsAtIndexPaths:[paths copy] withRowAnimation:UITableViewRowAnimationFade];
            }
            
        }else {
            NSString *d = [[NSString alloc] initWithData:_responseData encoding:NSUTF8StringEncoding];
            NSLog(d);
        }
    }
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    // The request has failed for some reason!
    // Check the error var
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Login error"
                                                    message:[error localizedDescription]
                                                   delegate:nil
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];
}

@end
