//
//  MonthStatsViewController.m
//  Precision iOS
//
//  Created by Zach Kauffman on 6/4/14.
//  Copyright (c) 2014 Zach Kauffman. All rights reserved.
//

#import "MonthStatsViewController.h"
#import "HoursByMatterViewController.h"
#import "GraphViewController.h"
#import "AppDelegate.h"
#import "ViewController.h"
@interface MonthStatsViewController ()

@end

@implementation MonthStatsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void) viewDidAppear:(BOOL)animated
{
    self.parentViewController.title = @"Yearly Stats";
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    _hourView.layer.cornerRadius = 5.0f;
    _entriesView.layer.cornerRadius = 5.0f;
    _mattersView.layer.cornerRadius = 5.0f;
    _goalView.layer.cornerRadius = 5.0f;
	// Do any additional setup after loading the view.
    CGAffineTransform transform = CGAffineTransformMakeScale(1.0f, 5.0f);
    _goalProgressBar.transform = transform;
    NSString *url = [NSString stringWithFormat: @"https://precisiontimeentry.com/api/get_time_stats.php?period=year"];
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:url]];
    
    // Create url connection and fire request
    NSURLConnection *conn = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    
    _spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    _spinner.opaque = NO;
    _spinner.backgroundColor = [UIColor colorWithWhite:0.0f alpha:0.6f];
    [_spinner setBounds:self.view.bounds];
    _spinner.center = self.view.center;
    _spinner.activityIndicatorViewStyle = UIActivityIndicatorViewStyleGray;
    [_spinner setColor:[UIColor colorWithRed:0.6 green:0.8 blue:1.0 alpha:1.0]];
    [self.view addSubview:_spinner]; // spinner is not visible until started
    [_spinner startAnimating];
    
    BOOL isGoalProgressEnabled = [[NSUserDefaults standardUserDefaults] boolForKey:@"weeklyTracking"];
    if(!isGoalProgressEnabled) {
        [_goalView setHidden:YES];
    }
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(imageTapDetected)];
    singleTap.numberOfTapsRequired = 1;
    
    UITapGestureRecognizer *singleTap2 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(imageTapDetected)];
    singleTap2.numberOfTapsRequired = 1;
    
    _hoursByMatterLabel.userInteractionEnabled = YES;
    [_hoursByMatterLabel addGestureRecognizer:singleTap];
    
    _hoursByMatterImage.userInteractionEnabled = YES;
    [_hoursByMatterImage addGestureRecognizer:singleTap2];
    
    UITapGestureRecognizer *entriesTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(entriesTapDetected)];
    entriesTap.numberOfTapsRequired = 1;
    
    UITapGestureRecognizer *entriesTap2 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(entriesTapDetected)];
    entriesTap2.numberOfTapsRequired = 1;
    
    _entriesByMatterImage.userInteractionEnabled = YES;
    [_entriesByMatterImage addGestureRecognizer:entriesTap];
    
    _entriesByMatterLabel.userInteractionEnabled = YES;
    [_entriesByMatterLabel addGestureRecognizer:entriesTap2];
    
    UITapGestureRecognizer *hoursByWeek = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hoursByWeekPressed)];
    hoursByWeek.numberOfTapsRequired = 1;
    
    UITapGestureRecognizer *hoursByWeek2 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hoursByWeekPressed)];
    hoursByWeek2.numberOfTapsRequired = 1;
    
    _hoursByMonthImage.userInteractionEnabled = YES;
    [_hoursByMonthImage addGestureRecognizer:hoursByWeek];
    
    _hoursByMonthLabel.userInteractionEnabled = YES;
    [_hoursByMonthLabel addGestureRecognizer:hoursByWeek2];
    
    UITapGestureRecognizer *entriesByDayTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(entriesByDayPressed)];
    entriesByDayTap.numberOfTapsRequired = 1;
    
    UITapGestureRecognizer *entriesByDayTap2 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(entriesByDayPressed)];
    entriesByDayTap2.numberOfTapsRequired = 1;
    
    _entriesByMonthImage.userInteractionEnabled = YES;
    [_entriesByMonthImage addGestureRecognizer:entriesByDayTap];
    
    _entriesByMonthLabel.userInteractionEnabled = YES;
    [_entriesByMonthLabel addGestureRecognizer:entriesByDayTap2];
    
    UITapGestureRecognizer *mattersByDayTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(mattersByDayPressed)];
    mattersByDayTap.numberOfTapsRequired = 1;
    
    UITapGestureRecognizer *mattersByDayTap2 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(mattersByDayPressed)];
    mattersByDayTap2.numberOfTapsRequired = 1;
    
    
    _mattersByMonthImage.userInteractionEnabled = YES;
    [_mattersByMonthImage addGestureRecognizer:mattersByDayTap];
    
    _mattersByMonthLabel.userInteractionEnabled = YES;
    [_mattersByMonthLabel addGestureRecognizer:mattersByDayTap2];
    
}
-(void)imageTapDetected{
    AppDelegate *appDelegate;
    appDelegate=(AppDelegate *)[[UIApplication sharedApplication] delegate];//in didLoad method
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:appDelegate.storyboardName bundle:nil];
    HoursByMatterViewController *v = [storyboard instantiateViewControllerWithIdentifier:@"hoursByMatter"];
    v.period = @"year";
    [self.navigationController pushViewController:v animated:YES];
    
}

-(void)entriesTapDetected{
    AppDelegate *appDelegate;
    appDelegate=(AppDelegate *)[[UIApplication sharedApplication] delegate];//in didLoad method
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:appDelegate.storyboardName bundle:nil];
    HoursByMatterViewController *v = [storyboard instantiateViewControllerWithIdentifier:@"entriesByMatter"];
    v.period = @"year";
    [self.navigationController pushViewController:v animated:YES];
    
}

-(void) hoursByWeekPressed
{
    AppDelegate *appDelegate;
    appDelegate=(AppDelegate *)[[UIApplication sharedApplication] delegate];//in didLoad method
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:appDelegate.storyboardName bundle:nil];
    GraphViewController *v = [storyboard instantiateViewControllerWithIdentifier:@"graphView"];
    v.period = @"month";
    v.type = @"hours";
    
    [self.navigationController pushViewController:v animated:YES];
}

-(void) entriesByDayPressed
{
    AppDelegate *appDelegate;
    appDelegate=(AppDelegate *)[[UIApplication sharedApplication] delegate];//in didLoad method
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:appDelegate.storyboardName bundle:nil];
    GraphViewController *v = [storyboard instantiateViewControllerWithIdentifier:@"graphView"];
    v.period = @"month";
    v.type = @"entries";
    [self.navigationController pushViewController:v animated:YES];
}

-(void) mattersByDayPressed
{
    AppDelegate *appDelegate;
    appDelegate=(AppDelegate *)[[UIApplication sharedApplication] delegate];//in didLoad method
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:appDelegate.storyboardName bundle:nil];
    GraphViewController *v = [storyboard instantiateViewControllerWithIdentifier:@"graphView"];
    v.period = @"month";
    v.type = @"matters";
    [self.navigationController pushViewController:v animated:YES];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark NSURLConnection Delegate Methods

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    // A response has been received, this is where we initialize the instance var you created
    // so that we can append data to it in the didReceiveData method
    // Furthermore, this method is called each time there is a redirect so reinitializing it
    // also serves to clear it
    _responseData = [[NSMutableData alloc] init];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    // Append the new data to the instance variable you declared
    [_responseData appendData:data];
}

- (NSCachedURLResponse *)connection:(NSURLConnection *)connection
                  willCacheResponse:(NSCachedURLResponse*)cachedResponse {
    // Return nil to indicate not necessary to store a cached response for this connection
    return nil;
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    // The request is complete and data has been received
    // You can parse the stuff in your instance variable now
    [_spinner stopAnimating];
    NSError *jsonParsingError = nil;
    id object = [NSJSONSerialization JSONObjectWithData:_responseData options:0 error:&jsonParsingError];
    
    if (jsonParsingError) {
        NSLog(@"JSON ERROR: %@", [jsonParsingError localizedDescription]);
    } else {
        if([object isKindOfClass:[NSDictionary class]]) {
            //trying to delete?
            NSDictionary *results = object;
            if([results objectForKey:@"stats"] != nil) {
                NSDictionary *set = [results objectForKey:@"stats"];
                [_hoursLabel setText:[NSString stringWithFormat:@"Hours this year: %@", [set objectForKey:@"total_hours"]]];
                
                [_entriesLabel setText:[NSString stringWithFormat:@"Entries this year: %@", [set objectForKey:@"total_entries"]]];
                
                [_mattersLabel setText:[NSString stringWithFormat:@"Matters this year: %@", [set objectForKey:@"total_matters"]]];
                
                float weekHours = [[NSUserDefaults standardUserDefaults] integerForKey:@"weeklyGoalValue"];
                float maxHours = 48*weekHours;
                NSString *hrs = [set objectForKey:@"total_hours"];
                float total_hours = [hrs floatValue];
                float percentage = (float)total_hours/maxHours;
                [_goalProgressBar setProgress:percentage];
                
                _goalProgressLabel.text = [NSString stringWithFormat:@"Yearly Goal Progress: %.f%%", percentage*100];
                
                
            }
        }else {
            NSString *d = [[NSString alloc] initWithData:_responseData encoding:NSUTF8StringEncoding];
        }
    }
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    // The request has failed for some reason!
    // Check the error var
    [_spinner stopAnimating];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Login error"
                                                    message:[error localizedDescription]
                                                   delegate:nil
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];
}


@end
