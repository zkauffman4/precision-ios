//
//  DayStatsViewController.h
//  Precision iOS
//
//  Created by Zach Kauffman on 6/4/14.
//  Copyright (c) 2014 Zach Kauffman. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DayStatsViewController : UIViewController
@property (strong, nonatomic) IBOutlet UIView *hours;
@property (strong, nonatomic) IBOutlet UIView *entries;
@property (strong, nonatomic) IBOutlet UIView *matters;
@property (strong, nonatomic) IBOutlet UIView *goal;
@property (strong, nonatomic) IBOutlet UIProgressView *progressBar;
@property (strong, nonatomic) IBOutlet UILabel *hoursLabel;
@property (strong, nonatomic) IBOutlet UILabel *entriesLabel;
@property (strong, nonatomic) IBOutlet UILabel *mattersLabel;
@property (strong, nonatomic) IBOutlet UILabel *goalProgressLabel;
@property (strong, nonatomic) NSMutableData *responseData;
@property (strong, nonatomic) IBOutlet UILabel *hoursByMatterLabel;
@property (strong, nonatomic) IBOutlet UIImageView *hoursByMatterImage;
@property (strong, nonatomic) IBOutlet UIImageView *entriesByMatterImage;
@property (strong, nonatomic) IBOutlet UILabel *entriesByMatterLabel;
@property (strong, nonatomic) IBOutlet UIImageView *hoursByDayImage;
@property (strong, nonatomic) IBOutlet UILabel *hoursByDayLabel;
@property (strong, nonatomic) IBOutlet UIImageView *entriesByDayImage;
@property (strong, nonatomic) IBOutlet UILabel *entriesByDayLabel;
@property (strong, nonatomic) IBOutlet UIImageView *mattersByDayImage;
@property (strong, nonatomic) IBOutlet UILabel *mattersByDayLabel;
@property (strong, nonatomic) UIActivityIndicatorView *spinner;
-(IBAction)hoursByDayPressed:(id)sender;
@end
