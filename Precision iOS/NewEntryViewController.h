//
//  NewEntryViewController.h
//  Precision iOS
//
//  Created by Zach Kauffman on 5/30/14.
//  Copyright (c) 2014 Zach Kauffman. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MatterObject.h"
#import "TaskCodeObject.h"
#import "MatterSearchViewController.h"
#import "TaskCodeViewController.h"
#import "ComponentSearchViewController.h"
#import "EntryObject.h"


@interface NewEntryViewController : UIViewController<UIPickerViewDelegate, UIPickerViewDataSource, UIScrollViewDelegate, UITextViewDelegate, UITextFieldDelegate, UITableViewDelegate, UISearchBarDelegate, UISearchDisplayDelegate>
//date picker
@property (strong, nonatomic) IBOutlet UISearchBar *matterSearchBar;
@property (strong, nonatomic) IBOutlet UITextField *dateTextField;
@property (strong, nonatomic) UIDatePicker *datePicker;
@property (strong, nonatomic) NSDate *date;
@property (strong, nonatomic) UIToolbar *keyboardToolbar;
@property (strong, nonatomic) IBOutlet UIView *taskView;
@property (strong, nonatomic) IBOutlet UILabel *matterSearchLabel;
@property (strong, nonatomic) IBOutlet UILabel *tCodeLabel;

@property (strong, nonatomic) IBOutlet UIView *submitView;
@property (strong, nonatomic) UIImageView *arrowImageView;
@property (strong, nonatomic) NSArray *dataArray;
//hour picker
@property (strong, nonatomic) IBOutlet UITextField *hoursField;
@property (strong, nonatomic) UIPickerView *myPickerView;
@property (strong, nonatomic) NSArray *hoursArray;
@property (strong, nonatomic) NSArray *minutesArray;
@property (strong, nonatomic) NSArray *titleArray;
@property (strong, nonatomic) NSString *selectedHour;
@property (strong, nonatomic) NSString *selectedMinute;
@property (weak, nonatomic) IBOutlet UIButton *matterSearchBtn;
@property (strong, nonatomic) IBOutlet UIButton *taskCodeBtn;
@property (strong, nonatomic) IBOutlet UIButton *componentCodeBtn;
@property CGFloat taskStartingY, submitStartingY;
@property (strong, nonatomic) MatterSearchViewController *vc;
@property (strong, nonatomic) IBOutlet UILabel *componentLabel;
@property (strong, nonatomic) NSString *te;
@property (strong, nonatomic) UIActivityIndicatorView *spinner;
@property BOOL isIncomplete, cloning;

@property (strong, nonatomic) IBOutlet UIImageView *cCodeArrow;
//description
@property (strong, nonatomic) IBOutlet UITextView *descriptionText;

//scroll view
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property CGFloat scrollDistance;
@property BOOL isScrolled;

//form fields
@property (strong, nonatomic) IBOutlet UITextField *taskField;
@property (strong, nonatomic) IBOutlet UITextField *componentField;
@property (strong, nonatomic) IBOutlet UISwitch *switchField;

//search data
@property (strong, nonatomic) MatterObject *selectedMatter;
-(void)setMatter: (MatterObject*) m;

-(void)setTimerValue: (NSString*) str;

@property (strong, nonatomic) TaskCodeObject *selectedTask;
-(void)setTask: (TaskCodeObject*) t;

@property (strong, nonatomic) TaskCodeObject *selectedComponent;
-(void)setComponent: (TaskCodeObject*) t;

@property (strong, nonatomic) EntryObject *editingEntry;
-(void)setEditingEntry:(EntryObject *)editingEntry;

-(void)setCloningEntry:(EntryObject*)cloningEntry;

//buttons
-(IBAction)cancelBtnPressed:(id)sender;
-(IBAction)enterBtnPressed:(id)sender;
-(IBAction)matterSearchBtnPressed:(id)sender;
-(IBAction)taskCodeSearchBtnPressed:(id)sender;
-(IBAction)componentCodeSearchBtnPressed:(id)sender;
-(IBAction)stopWatchPressed:(id)sender;

//matter

//response
@property (strong, nonatomic) NSMutableData *responseData;

@end
