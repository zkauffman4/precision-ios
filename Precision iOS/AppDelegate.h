//
//  AppDelegate.h
//  Precision iOS
//
//  Created by Zach Kauffman on 5/29/14.
//  Copyright (c) 2014 Zach Kauffman. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) NSString *storyboardName;
@property (strong, nonatomic) UIImageView *splashView;
@end
