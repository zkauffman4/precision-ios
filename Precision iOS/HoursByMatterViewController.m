//
//  HoursByMatterViewController.m
//  Precision iOS
//
//  Created by Zach Kauffman on 6/5/14.
//  Copyright (c) 2014 Zach Kauffman. All rights reserved.
//
//NOTE: in this viewcontroller, we use matter.tGroup as total_hours so we don't have to create a new object

#import "HoursByMatterViewController.h"
#import "MatterObject.h"
#import "ViewController.h"
#import "AppDelegate.h"

@interface HoursByMatterViewController ()

@end

@implementation HoursByMatterViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    if([_period isEqualToString:@"week"]) {
        self.navigationItem.title = @"Hours by matter (week)";
    } else if([_period isEqualToString:@"month"]) {
        self.navigationItem.title = @"Hours by matter (month)";
    } else {
        self.navigationItem.title = @"Hours by matter (YTD)";
    }
    NSString *url = [NSString stringWithFormat: @"https://precisiontimeentry.com/api/get_matter_hours.php?period=%@", _period];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]];
    NSURLConnection *conn = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    
    _spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    _spinner.opaque = NO;
    _spinner.backgroundColor = [UIColor colorWithWhite:0.0f alpha:0.6f];
    [_spinner setBounds:self.view.bounds];
    _spinner.center = self.view.center;
    _spinner.activityIndicatorViewStyle = UIActivityIndicatorViewStyleGray;
    [_spinner setColor:[UIColor colorWithRed:0.6 green:0.8 blue:1.0 alpha:1.0]];
    [self.view addSubview:_spinner]; // spinner is not visible until started
    [_spinner startAnimating];
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    _searchBar.delegate = (id)self;
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    if(_isFiltered) {
        return [_filteredTableData count];
    }
    return [_mattersArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    //[self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"MatterCell"];
    static NSString *CellIdentifier = @"MatterCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell==nil) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        
    }
    
    if(_isFiltered) {
        cell.textLabel.text = ((MatterObject*)[_filteredTableData objectAtIndex:indexPath.row]).matterName;
        cell.detailTextLabel.text = [NSString stringWithFormat:@"Hours: %@",((MatterObject*)[_filteredTableData objectAtIndex:indexPath.row]).tGroup];
        
    } else {
        cell.textLabel.text = ((MatterObject*)[_mattersArray objectAtIndex:indexPath.row]).matterName;
        cell.detailTextLabel.text = [NSString stringWithFormat:@"Hours: %@",((MatterObject*)[_mattersArray objectAtIndex:indexPath.row]).tGroup];
        
    }
    // Configure the cell...
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}

-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    _isFiltered = NO;
    [self.tableView reloadData];
}

#pragma mark NSURLConnection Delegate Methods

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    // A response has been received, this is where we initialize the instance var you created
    // so that we can append data to it in the didReceiveData method
    // Furthermore, this method is called each time there is a redirect so reinitializing it
    // also serves to clear it
    _responseData = [[NSMutableData alloc] init];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    // Append the new data to the instance variable you declared
    [_responseData appendData:data];
}

- (NSCachedURLResponse *)connection:(NSURLConnection *)connection
                  willCacheResponse:(NSCachedURLResponse*)cachedResponse {
    // Return nil to indicate not necessary to store a cached response for this connection
    return nil;
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    // The request is complete and data has been received
    // You can parse the stuff in your instance variable now
    
    [_spinner stopAnimating];
    NSError *jsonParsingError = nil;
    id object = [NSJSONSerialization JSONObjectWithData:_responseData options:0 error:&jsonParsingError];
    
    if (jsonParsingError) {
        NSLog(@"JSON ERROR: %@", [jsonParsingError localizedDescription]);
    } else {
        if([object isKindOfClass:[NSDictionary class]]) {
            //trying to delete?
            NSDictionary *results = object;
            //NSLog(@"Data: %@", [results description]);
            if([results objectForKey:@"matters"]) {
                NSArray *matters = [results objectForKey:@"matters"];
                _mattersArray = [NSMutableArray array];
                for(NSDictionary *item in matters) {
                    NSString *matterName = [item objectForKey:@"matter_name"];
                    NSString *total_hours = [item objectForKey:@"total_hours"];
                    
                    MatterObject *matt = [[MatterObject alloc] initWithId:nil andName:matterName taskCodeGroup:total_hours componentCodeGroup:nil];
                    
                    [_mattersArray addObject:matt];
                }
                [self.tableView reloadData];
            } else if([object objectForKey:@"session"] != nil) {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Session Timeout"
                                                                message:@"You have had more than 15 minutes of inactivity. To protect your account, you have been logged out. Please log in again."
                                                               delegate:nil
                                                      cancelButtonTitle:@"OK"
                                                      otherButtonTitles:nil];
                [alert show];
                AppDelegate *appDelegate;
                appDelegate=(AppDelegate *)[[UIApplication sharedApplication] delegate];//in didLoad method
                UIStoryboard *storyboard = [UIStoryboard storyboardWithName:appDelegate.storyboardName bundle:nil];
                ViewController *v = [storyboard instantiateViewControllerWithIdentifier:@"loginController"];
                [self presentViewController:v animated:YES completion:nil];
            }

        } else {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Database error"
                                                            message:@"A database error has occurred"
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
        }
    }
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    // The request has failed for some reason!
    // Check the error var
    [_spinner stopAnimating];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Login error"
                                                    message:[error localizedDescription]
                                                   delegate:nil
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];
}

-(void)searchBar:(UISearchBar*)searchBar textDidChange:(NSString*)text
{
    if(text.length == 0)
    {
        _isFiltered = FALSE;
    }
    else
    {
        _isFiltered = true;
        _filteredTableData = [[NSMutableArray alloc] init];
        
        for (MatterObject* matter in _mattersArray)
        {
            NSRange nameRange = [matter.matterName rangeOfString:text options:NSCaseInsensitiveSearch];
            if(nameRange.location != NSNotFound)
            {
                [_filteredTableData addObject:matter];
            }
        }
    }
    
    [self.tableView reloadData];
}

@end
