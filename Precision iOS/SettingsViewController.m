//
//  SettingsViewController.m
//  Precision iOS
//
//  Created by Zach Kauffman on 6/4/14.
//  Copyright (c) 2014 Zach Kauffman. All rights reserved.
//

#import "SettingsViewController.h"
#import "FBEncryptorAES.h"
#import "ViewController.h"
#import "AppDelegate.h"
#import "ViewController.h"

@interface SettingsViewController ()

@end
//@j7tkqb26@j7tkqb2five
@implementation SettingsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(IBAction)sliderValueChanged:(id)sender
{
    if (sender == _weeklyGoalSlider) {
        float number = _weeklyGoalSlider.value;
        int val = (int)(number);
        _weeklyGoalLabel.text = [NSString stringWithFormat:@"%d hours", val];
        [[NSUserDefaults standardUserDefaults] setInteger:val forKey:@"weeklyGoalValue"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    
}

-(IBAction)switchValueChanged:(id)sender
{
    if(sender == _goalTrackingSwitch) {
        [[NSUserDefaults standardUserDefaults] setBool:_goalTrackingSwitch.isOn forKey:@"weeklyTracking"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    } else if(sender == _enabledEmailLogs) {
        [[NSUserDefaults standardUserDefaults] setBool:_enabledEmailLogs.isOn forKey:@"emailLogs"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        [_exchangeEmailTextfield setHidden:!_enabledEmailLogs.isOn];
        [_emailLogsText setHidden:!_enabledEmailLogs.isOn];
    } else if(sender == _zeroTime) {
        [[NSUserDefaults standardUserDefaults] setBool:_zeroTime.isOn forKey:@"zeroTime"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    //set all switches on load
    //goal tracking
    [_goalTrackingSwitch setOn:[[NSUserDefaults standardUserDefaults] boolForKey:@"weeklyTracking"]];
    
    [_zeroTime setOn:[[NSUserDefaults standardUserDefaults] boolForKey:@"zeroTime"]];
    
    //email logs switch
    [_enabledEmailLogs setOn:[[NSUserDefaults standardUserDefaults] boolForKey:@"emailLogs"]];
    [_exchangeEmailTextfield setHidden:!_enabledEmailLogs.isOn];
    [_emailLogsText setHidden:!_enabledEmailLogs.isOn];
    
    //set email text
    _exchangeEmailTextfield.text = [[NSUserDefaults standardUserDefaults] objectForKey:@"exchange_email"];
    _oldEmail = _exchangeEmailTextfield.text;
    
    NSUInteger hours = [[NSUserDefaults standardUserDefaults] integerForKey:@"weeklyGoalValue"];
    if(hours > 60 || hours < 1) {
        hours = 40;
    }
    
    [_emailLogsText setSecureTextEntry:YES];
    
    NSString *e = [[NSUserDefaults standardUserDefaults] objectForKey:@"e"];
    if(e != nil) {
        if([e length] > 0) {
            _emailLogsText.text = e;
        }
    }
    
    _emailLogsText.delegate = self;
    _exchangeEmailTextfield.delegate = self;
    _weeklyGoalLabel.text = [NSString stringWithFormat:@"%lu hours", (unsigned long)hours];
    _weeklyGoalSlider.value = hours;
    
    _spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    _spinner.opaque = NO;
    _spinner.backgroundColor = [UIColor colorWithWhite:0.0f alpha:0.6f];
    [_spinner setBounds:self.view.bounds];
    _spinner.center = self.view.center;
    _spinner.activityIndicatorViewStyle = UIActivityIndicatorViewStyleGray;
    [_spinner setColor:[UIColor colorWithRed:0.6 green:0.8 blue:1.0 alpha:1.0]];
    [self.view addSubview:_spinner]; // spinner is not visible until started

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    return YES;
}

-(IBAction)doneBtnPressed:(id)sender
{
    if(_enabledEmailLogs.isOn) {
        if(![_emailLogsText.text isEqualToString:[[NSUserDefaults standardUserDefaults] objectForKey:@"e"]] && ![_emailLogsText.text isEqualToString:@""]) {
            //encrypt password and save to phone
            NSString *encrypted = [FBEncryptorAES encryptBase64String:_emailLogsText.text keyString:@"@j7tkqb26" separateLines:NO];
            
            [[NSUserDefaults standardUserDefaults] setObject:encrypted forKey:@"e"];
            [[NSUserDefaults standardUserDefaults] synchronize];
        } else if([_emailLogsText.text isEqualToString:@""]) {
            [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"e"];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
    }
    if(![_oldEmail isEqualToString:_exchangeEmailTextfield.text]) {
        //new email, update email in db
        NSString *url = [NSString stringWithFormat: @"https://precisiontimeentry.com/api/update_email.php"];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]];
        [request setHTTPMethod:@"POST"];
        NSString *postString = [NSString stringWithFormat:@"email=%@", _exchangeEmailTextfield.text];
        [request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
        
        // Create url connection and fire request
        NSURLConnection *conn = [[NSURLConnection alloc] initWithRequest:request delegate:self];
        _oldEmail = _exchangeEmailTextfield.text;
        [_spinner startAnimating];
    } else {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

#pragma mark NSURLConnection Delegate Methods

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    // A response has been received, this is where we initialize the instance var you created
    // so that we can append data to it in the didReceiveData method
    // Furthermore, this method is called each time there is a redirect so reinitializing it
    // also serves to clear it
    _responseData = [[NSMutableData alloc] init];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    // Append the new data to the instance variable you declared
    [_responseData appendData:data];
}

- (NSCachedURLResponse *)connection:(NSURLConnection *)connection
                  willCacheResponse:(NSCachedURLResponse*)cachedResponse {
    // Return nil to indicate not necessary to store a cached response for this connection
    return nil;
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    // The request is complete and data has been received
    // You can parse the stuff in your instance variable now
    
    NSError *jsonParsingError = nil;
    id object = [NSJSONSerialization JSONObjectWithData:_responseData options:0 error:&jsonParsingError];
    [_spinner stopAnimating];
    if (jsonParsingError) {
        NSLog(@"JSON ERROR: %@", [jsonParsingError localizedDescription]);
    } else {
        if([object isKindOfClass:[NSDictionary class]])
        {
            if([object objectForKey:@"session"] != nil) {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Session Timeout"
                                                                message:@"You have had more than 15 minutes of inactivity. To protect your account, you have been logged out. Please log in again."
                                                               delegate:nil
                                                      cancelButtonTitle:@"OK"
                                                      otherButtonTitles:nil];
                [alert show];
                AppDelegate *appDelegate;
                appDelegate=(AppDelegate *)[[UIApplication sharedApplication] delegate];//in didLoad method
                UIStoryboard *storyboard = [UIStoryboard storyboardWithName:appDelegate.storyboardName bundle:nil];
                ViewController *v = [storyboard instantiateViewControllerWithIdentifier:@"loginController"];
                [self presentViewController:v animated:YES completion:nil];
            } else {

                int responseCode = [[object objectForKey:@"response_code"] intValue];
                if(responseCode == 1) {
                    [[NSUserDefaults standardUserDefaults] setObject:_oldEmail forKey:@"exchange_email"];
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Save Successful"
                                                                    message:@"Your settings have been saved!"
                                                                   delegate:nil
                                                          cancelButtonTitle:@"OK"
                                                          otherButtonTitles:nil];
                    [alert show];
                    [self.navigationController popViewControllerAnimated:YES];
                } else if(responseCode == 2) {
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Save error"
                                                                    message:@"There was an error saving your settings. Please try again. If the problem persists, please contact an administrator."
                                                                   delegate:nil
                                                          cancelButtonTitle:@"OK"
                                                          otherButtonTitles:nil];
                    [alert show];
                }else {
                    //invalid server response
                }
            }
        }
        else {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Server error"
                                                            message:@"A server error has occured, please try again"
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
        }
    }
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    // The request has failed for some reason!
    // Check the error var
    [_spinner stopAnimating];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Login error"
                                                    message:[error localizedDescription]
                                                   delegate:nil
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];
}

@end
