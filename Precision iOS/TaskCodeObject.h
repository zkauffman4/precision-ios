//
//  TaskCodeObject.h
//  Precision iOS
//
//  Created by Zach Kauffman on 6/2/14.
//  Copyright (c) 2014 Zach Kauffman. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TaskCodeObject : NSObject
@property (nonatomic, strong) NSString *codeId;
@property (nonatomic, strong) NSString *codeName;

-(TaskCodeObject*) initWithId: (NSString*)codeId andName: (NSString*)codeName;
@end