//
//  EntryObject.m
//  Precision iOS
//
//  Created by Zach Kauffman on 5/29/14.
//  Copyright (c) 2014 Zach Kauffman. All rights reserved.
//

#import "EntryObject.h"

@implementation EntryObject

-(id)initWithJSONData:(NSDictionary*)data{
    self = [super init];
    if(self){
        //NSLog(@"initWithJSONData method called");
        self.entryId = [data objectForKey:@"id"];
        self.matterId =  [data objectForKey:@"matter_id"];
        self.description = [data objectForKey:@"description"];
        self.lcode = [data objectForKey:@"task_code_id"];
        self.acode = [data objectForKey:@"component_code_id"];
        self.date = [data objectForKey:@"date_worked"];
        self.hours = [data objectForKey:@"hours_worked"];
        self.matterName = [data objectForKey:@"matter_name"];
        self.lcodeGroup = [data objectForKey:@"task_group"];
        self.acodeGroup = [data objectForKey:@"component_group"];

    }
    return self;
}

@end
